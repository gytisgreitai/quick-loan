package eu.greitai.ql.settings;

import java.math.RoundingMode;

/**
 * 
 * Project constants. Should not be changed without runing the tests.
 *
 * @author gytis
 */
public class Constants {
    
    /***
     * How many numbers do we keep for users to display?
     */
    public final static int VISIBLE_SCALE = 2;
    
    /**
     * How do we round when scaling for user display?
     */
    public final static RoundingMode VISIBLE_ROUNDING_MODE = RoundingMode.HALF_UP;

    /**
     * How do we round when scaling for calculation?
     */
    public final static RoundingMode ALGO_ROUNDING_MODE = RoundingMode.HALF_UP;

}

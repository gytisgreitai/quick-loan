package eu.greitai.ql.settings;



/**
 * Various loan related settings.
 * 
 * @author gytis
 */
public enum LoanSettings implements Setting {
    /**
     * Minimum loan amount, in currency format.
     */
    AMOUNT_MIN,
    /**
     * Maximum loan amount, in currency format.
     */
    AMOUNT_MAX,
    /**
     * Initial suggested loan amount, in currency format.
     */
    AMOUNT_INITIAL,
    /**
     * Minimum loan duration, in days.
     */
    DURATION_MIN,
    /**
     * Maximum loan duration, in days.
     */
    DURATION_MAX,
    /**
     * Initial suggested loan duration in days.
     */
    DURATION_INITIAL,

    /**
     * Yearly erest rates, percentage.
     */
    YEARLY_INTEREST,

    /**
     * Days per year for erest rate calculation.
     * 
     * This is not entirely correct for leap year, but for the sake of simplicity use this
     * constant.
     */
    DAYS_PER_YEAR,

    /**
     * Daily loan erest rate.
     */
    DAILY_INTEREST_RATE,

    /**
     * When extending a loan, how much more interests should we pay?
     */
    EXTENSION_MULTIPLIER,

    /**
     * When extending loan, for how long can we extend it?
     */
    EXTENSION_TIME;
}

package eu.greitai.ql.dto;

import java.util.Set;

/**
 * 
 * Evaluation result of Loan risk application. This indicates if loan can be given to the user.
 * 
 * @author gytis
 */
public class LoanEvaluationResult {

    /**
     * Indicates if loan can be given out to the user.
     */
    private Result result;

    /**
     * Optionally might contain reason why evaluation has failed.
     */
    private Set<String> errors;

    public enum Result {
        /**
         * All ok
         */
        OK,
        /**
         * Risk is to high for user to give out the loan.
         */
        RISK_TOO_HIGH,
    }

    public Result getResult() {
        return result;
    }

    public void setResult(final Result result) {
        this.result = result;
    }

    public Set<String> getErrors() {
        return errors;
    }

    public void setErrors(final Set<String> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "LoanRiskResult [result=" + result + ", errors=" + errors + "]";
    }

}

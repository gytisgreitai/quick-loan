package eu.greitai.ql.dto;

import java.math.BigDecimal;

import org.joda.time.DateTime;

/**
 * 
 * Holds calculated loan information
 * 
 * @author gytis
 */
public class LoanCalculation {

    /**
     * Loan amount.
     */
    private BigDecimal amount;

    /**
     * Loan duration, in days.
     */
    private int duration;

    /**
     * Calculated interests for this loan.
     */
    private BigDecimal interests;

    /**
     * How much user must return?
     */
    private BigDecimal total;

    /**
     * End day, when loan must be returned?
     */
    private DateTime returnDate;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(final int duration) {
        this.duration = duration;
    }

    public BigDecimal getInterests() {
        return interests;
    }

    public void setInterests(final BigDecimal interests) {
        this.interests = interests;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(final BigDecimal total) {
        this.total = total;
    }

    public DateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(final DateTime returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public String toString() {
        return "LoanResult [amount=" + amount + ", duration=" + duration + ", interests=" + interests + ", total=" + total + ", returnDate="
                + returnDate + "]";
    }

}

package eu.greitai.ql.dto;

/**
 * 
 * Represents loan application result - outcome of user application.
 * 
 * 
 * 
 * @author gytis
 */
public class LoanApplicationResult {

    /**
     * Outcome status
     */
    private Status status;

    /**
     * Calculated loan information.
     */
    private LoanCalculation loan;


    public enum Status {
        OK,
        PRICE_MISMATCH,
        DECLINED
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public LoanCalculation getLoan() {
        return loan;
    }

    public void setLoan(final LoanCalculation loan) {
        this.loan = loan;
    }

    @Override
    public String toString() {
        return "LoanApplicationResult [status=" + status + ", loan=" + loan + "]";
    }

}

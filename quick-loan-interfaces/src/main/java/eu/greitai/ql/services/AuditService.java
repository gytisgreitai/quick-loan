package eu.greitai.ql.services;

import org.joda.time.DateTime;

import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.security.SecurityContext;

/**
 * 
 * Logs system events and can be used to retrieve them.
 * 
 * @author gytis
 */
public interface AuditService {

    /**
     * Audits an event
     * 
     * @param context - currently executing context.
     * @param event - event to audit
     * @param data - any additional data to log.
     */
    public void audit(SecurityContext context, Event event, String data);

    /**
     * Gets event count for given period and ip address.
     * 
     * @param event - audited event
     * @param ipAddress - ip address in which event has occured
     * @param start - start date when events should be searched, inclusive
     * @param end - end date when events should be searched, inclusive
     * @return - how many events have occured.
     */
    public Long getCountByEventTypeForIpPerPeriod(
            final Event event,
            final String ipAddress,
            final DateTime start,
            final DateTime end);

}

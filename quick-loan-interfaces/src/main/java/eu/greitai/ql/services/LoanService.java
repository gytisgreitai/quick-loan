package eu.greitai.ql.services;

import java.math.BigDecimal;
import java.util.UUID;

import eu.greitai.ql.dto.LoanApplicationResult;
import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.security.SecurityContext;

/**
 * 
 * Manages loan applications, extensions and calculation.
 * 
 * @author gytis
 */
public interface LoanService {

    /**
     * Calculates loan for given duration and amount
     * 
     * @param duration - loan duration, in days.
     * @param amount - loan amount that user wants to take
     * @param context - current users context.
     * @return {@link LoanCalculation} - calculated loan data.
     */
    public LoanCalculation calculate(final SecurityContext context, final int duration, final BigDecimal amount);

    /**
     * Applies for a loan
     * 
     * @param duration - loan duration, in days.
     * @param amount - loan amount that user wants to take
     * @param seenPrice - what total price user has seen?
     * @param context - current users context.
     * @return {@link LoanApplicationResult} - result indicating if user got the loan and exact details.
     */
    public LoanApplicationResult apply(
            final SecurityContext context,
            final int duration,
            final BigDecimal amount,
            final BigDecimal seenPrice);

    /**
     * Extends given loan for a predefined period.
     * 
     * @param context - current users context.
     * @param loanGuid - loan guid to extend. {@link Loan#getLoanGuid()}
     * @return - recalculated loan.
     */
    public LoanCalculation extend(final SecurityContext context, UUID loanGuid);

}

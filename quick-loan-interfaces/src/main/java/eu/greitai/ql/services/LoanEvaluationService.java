package eu.greitai.ql.services;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.dto.LoanEvaluationResult;
import eu.greitai.ql.security.SecurityContext;

/**
 * 
 * Responsible for loan evaluation and risk calculation when applying for a loan..
 * 
 * @author gytis
 */
public interface LoanEvaluationService {

    /**
     * Evaluates if loan can be given out to the user with given loan date.
     * 
     * @param loanApplication - loan to which user is applying.
     * @param context - current users context.
     * @return {@link LoanEvaluationResult} loan evaluation result
     */
    LoanEvaluationResult evaluate(final SecurityContext context, final LoanCalculation loanApplication);
}

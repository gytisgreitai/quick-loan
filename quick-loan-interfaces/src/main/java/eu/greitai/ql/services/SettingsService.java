package eu.greitai.ql.services;

import java.math.BigDecimal;
import java.util.Map;

import eu.greitai.ql.settings.Setting;

/**
 * 
 * Settings storage.
 * 
 * @author gytis
 */
public interface SettingsService {

    BigDecimal getAsDecimal(final Setting setting);
    
    Integer getAsInt(final Setting setting);

    Object get(final Setting setting);

    Map<Setting, Object> getAll();

}

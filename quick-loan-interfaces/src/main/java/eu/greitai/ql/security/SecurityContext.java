package eu.greitai.ql.security;

import java.util.Map;

/**
 * 
 * Execution security context - holds user data if applicable and any other information.
 * 
 * @author gytis
 */
public interface SecurityContext {

    /**
     * Returns user id associated with this context.
     * 
     * @return id of user, or null if user is anonymous.
     */
    public Long getUserId();

    /**
     * Meta data bag. Returns any additional meta data associated with this context.
     * Always not null.
     * 
     * @return metadata associated with this context.
     */
    public Map<String, Object> getMetaData();

}

/**
 * Poor mans Javascript E2E test runner. Bsically this is a big hack to run angularjs protractor
 * for our integration tests. What it does:
 * 
 * 1) Starts up selenium serer on standard port - 4444
 * 2) Executes protractor
 * 3) Checks for output, and fails build if there were any protractor errors.
 * 
 * The good way to do it would be to use maven-selenium plugin, but somehow (maybe mac related) i cannot pass in chrome driver
 * (link http://stackoverflow.com/q/19724016/911849) and also chromedriver is having freezes on mavericks 
 * (see https://code.google.com/p/chromedriver/issues/detail?id=590) so we cannot have maven plugin downloader
 * 
 * Protractor has the ability to start selenium by its own, by using seleniumJarFile, but this does not play nicely with
 * maven multi module projects, as path is related to directory from which protractor is executed.
 * 
 * Script should be run from gmaven plugin and excepts the following parameters:
 * 1) protractorScript - where is the protractor config located
 * 2) binaryPath - where are the chrome drivers located.
 * 3) seleniumServer - where is the standalone selenium jar located.
 */

import java.io.*

//get pathbuilder that will create paths for our drivers
def pather = new DriverPathBuilder(project.properties.binaryPath)
//create selenium
def seleniumCmd = """java -jar ${project.properties.seleniumServer}
 -Dphantomjs.binary.path=${pather.getPath('phantomjs', project.properties.phantomVersion)}  
 -Dwebdriver.chrome.driver=${pather.getPath('googlechrome', project.properties.chromeVersion)} 
 -Dwebdriver.ie.driver=${pather.getPath('internetexplorer', project.properties.ieVersion)}"""


//startup selenium and wait until it initializes by simply printing out the output.
def selenium = seleniumCmd.execute()
BufferedReader br = new BufferedReader(new InputStreamReader(selenium.getInputStream()))
def line
while ((line = br.readLine()) != null) {
	println line
	if(line.contains("Started org.openqa.jetty.jetty.Server")) {
		break;
	}
}

//run the protractor tests.
def prefix = System.getProperties().getProperty("os.name").toLowerCase().contains("windows") ? "cmd /c" : "";

def protractor = "${prefix} protractor ${project.properties.protractorScript}".execute()
protractor.waitFor()

//shutdown selenium
selenium.destroy()

//check if all went well, if so - we should have something like 1 test, 1 assertion, 0 failures
def stdout  = protractor.in.text
println stdout + protractor.err.text;

//fail the build if we had any build failures
if(!stdout.contains('0 failures')) {
	fail("Javascript e2e integration test exceution failed, there are protractor errors. Check the logs.")
}

/**
 * Creates paths to various selenium drivers.
 * 
 * @author gytis
 *
 */
class DriverPathBuilder {

	String architecture
	String os
	String path
	Map executables = ["googlechrome" : "chromedriver", "phantomjs" : "phantomjs", "internetexplorer": "IEDriverServer"]

	DriverPathBuilder(String basePath) {
		path = basePath

		architecture = (System.getProperties().getProperty("os.arch").toLowerCase().equals("x86_64") ||
		System.getProperties().getProperty("os.arch").toLowerCase().equals("amd64"))  ? "64bit" : "32bit"

		os = System.getProperties().getProperty("os.name").toLowerCase().contains("windows") ? "windows" :
		System.getProperties().getProperty("os.name").toLowerCase().contains("mac") ? "osx" : "linux"
	}

	/**
	 * Gets full path for driver defined by type and its version.
	 */
	String getPath(String type, String version) {
		"${path}/${os}/${type}/${architecture}/${version}/${executables[type]}" + (os == "windows" ? ".exe" : "")
	}
}
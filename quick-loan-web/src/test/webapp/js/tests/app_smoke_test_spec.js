describe('Our home page', function() {
	it('should contain correct H3 header', function() {
		browser.get('http://localhost:8080');

		var greeting = element(by.tagName('h3'));

		expect(greeting.getText()).toEqual('Quick Loan Solutions');
	});

	it('Active menu entries should be highlighted', function() {
		browser.get('http://localhost:8080');

		expect(element(by.css("ul.nav li.active a")).getText()).toEqual('Home');
		
		browser.get('http://localhost:8080#/my-loans');

		expect(element(by.css("ul.nav li.active a")).getText()).toEqual('My Loans');
	});
	
	it('Not found route should redirec to main', function() {
		browser.get('http://localhost:8080#/i/guess/this/route/does/not/exist');

		expect(element(by.css("ul.nav li.active a")).getText()).toEqual('Home');
	});

});
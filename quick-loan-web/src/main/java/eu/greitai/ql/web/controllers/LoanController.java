package eu.greitai.ql.web.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.security.SecurityContext;
import eu.greitai.ql.services.LoanService;
import eu.greitai.ql.web.dto.LoanCalculateRequest;

@Controller
public class LoanController {

    @Autowired
    private LoanService loanService;

    @RequestMapping(value = "/api/private/calculate")
    @ResponseBody
    public LoanCalculation calculate(
            @RequestBody final LoanCalculateRequest request) {
        return loanService.calculate(new SecurityContext() {

            @Override
            public Long getUserId() {
                // TODO Auto-generated method stub
                return 1L;
            }

            @Override
            public Map<String, Object> getMetaData() {
                // TODO Auto-generated method stub
                Map<String, Object> result = Maps.newHashMap();
                result.put("ipAddress", "127.0.0.1");
                return result;
            }
        }, request.getDuration(), request.getAmount());
    }

}

package eu.greitai.ql.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import eu.greitai.ql.security.auth.provider.UsernamePasswordAuthProvider;
import eu.greitai.ql.web.security.ApiLoginUrlAuthenticationEntryPoint;
import eu.greitai.ql.web.security.AuthFailureHandler;
import eu.greitai.ql.web.security.AuthSuccessHandler;
import eu.greitai.ql.web.security.CorsHeaderWriter;

/**
 * 
 * Enables spring security for our web module.
 * 
 * @author gytis
 */
@Configuration
@EnableWebSecurity
@ComponentScan("eu.greitai.ql.web.security")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UsernamePasswordAuthProvider usernamePassAuthProvider;

    @Autowired
    private AuthSuccessHandler successHandler;

    @Autowired
    private AuthFailureHandler failureHandler;

  
    //@formatter:off
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
        .headers()
            .addHeaderWriter(new CorsHeaderWriter())
        .and()
            .httpBasic()
                .authenticationEntryPoint(apiLoginUrlAuthenticationEntryPoint())
        .and()
            .csrf()
                .disable()
            // configure form login, as we are serving plain api
            // we do not need regular login form pages, so redirect to non existing ones, if someone tries to access
            .formLogin()
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                .loginPage("/api/non-existing-page")
                .loginProcessingUrl("/api/login")
                .usernameParameter("userName")
                .passwordParameter("password")
                .failureUrl("/api/non-existing-page").permitAll()
        .and()
            .authorizeRequests()
                .antMatchers("/api/private/**").authenticated()
                .antMatchers("/**").permitAll()
        .and()
            .addFilterBefore(getDefaultAuthenticationFilter(), AbstractPreAuthenticatedProcessingFilter.class)
            .logout()
                .deleteCookies("JSESSIONID")
                 .logoutUrl("/api/logout")
                 .logoutSuccessUrl("/api/login")
        .permitAll();
    }
    //@formatter:on

    /**
     * Enables username password authnetication filter.
     */
    @Bean
    public UsernamePasswordAuthenticationFilter getDefaultAuthenticationFilter() throws Exception {
        UsernamePasswordAuthenticationFilter filter = new UsernamePasswordAuthenticationFilter();
        filter.setAuthenticationManager(authenticationManagerBean());
        filter.setAuthenticationSuccessHandler(successHandler);
        filter.setAuthenticationFailureHandler(failureHandler);
        return filter;
    }

    /**
     * Ensures that we return 401 for unauthorized methods, instead of redirecting to login page.
     */
    @Bean
    public ApiLoginUrlAuthenticationEntryPoint apiLoginUrlAuthenticationEntryPoint() {
        return new ApiLoginUrlAuthenticationEntryPoint("/api/login");
    }

    /**
     * Wires up our custom username/password auth provider.

     */
    @Override
    protected void configure(final AuthenticationManagerBuilder builder) throws Exception {
        builder
        .authenticationProvider(usernamePassAuthProvider);

    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}

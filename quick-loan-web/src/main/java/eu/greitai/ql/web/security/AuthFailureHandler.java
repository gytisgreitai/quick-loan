package eu.greitai.ql.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import eu.greitai.ql.web.security.LoginResult.Status;

/**
 * 
 * Invoked when authentication fails - instead of redirect, we return JSON resut.
 * 
 * @author gytis
 */
@Component
public class AuthFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final AuthenticationException exception) throws IOException, ServletException {
        response.getWriter().write(new Gson().toJson(new LoginResult(Status.FAILED)));
    }

}

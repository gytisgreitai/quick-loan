package eu.greitai.ql.web.config;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 
 * Spring MVC and our web application central configuration.
 * 
 * @author gytis
 */
@Configuration
@EnableWebMvc
@ComponentScan({ "eu.greitai.ql.web.controllers" })
public class WebApplicationConfig extends WebMvcConfigurerAdapter {

    private static final Charset UTF8 = Charset.forName("UTF-8");

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public MappingJacksonHttpMessageConverter jacksonConverter() {
        MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
        return converter;
    }

    @Override
    public void configureContentNegotiation(final ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false);
        configurer.favorParameter(false);
        configurer.ignoreAcceptHeader(false);
        Map<String, MediaType> mediaTypes = new HashMap<>();
        mediaTypes.put("html", new MediaType("text", "html", UTF8));
        mediaTypes.put("json", new MediaType("application", "json", UTF8));
        mediaTypes.put("*", new MediaType("*", "*"));
        configurer.mediaTypes(mediaTypes);
        super.configureContentNegotiation(configurer);
    }

    @Override
    public void configureMessageConverters(final List<HttpMessageConverter<?>> converters) {
        StringHttpMessageConverter stringConverter = new StringHttpMessageConverter();
        stringConverter.setSupportedMediaTypes(Arrays.asList(new MediaType("text", "html", UTF8)));
        converters.add(stringConverter);
        converters.add(jacksonConverter());
        super.configureMessageConverters(converters);
    }

    @Bean
    public ObjectMapper jacksonObjectMapper() {
        return jacksonConverter().getObjectMapper();
    }
}

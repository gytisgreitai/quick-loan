package eu.greitai.ql.web.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.settings.Setting;

/**
 * End point for various settings related logic
 * 
 * @author gytis
 */
@Controller
public class SettingsController {

    @Autowired
    private SettingsService settingsService;
    /**
     * Gets system wide settings.
     * 
     * @return system wide settings in json format.
     */
    @RequestMapping(value = "/api/settings")
    @ResponseBody
    public Map<Setting, Object> settings() {
        return settingsService.getAll();
    }
}

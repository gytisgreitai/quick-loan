package eu.greitai.ql.web.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.header.HeaderWriter;

/**
 * 
 * Adds cross site request headers needed for our development grunt server.
 * 
 * FIXME: only issue this for DEV environment.
 * 
 * @author gytis
 */
public class CorsHeaderWriter implements HeaderWriter {

    @Override
    public void writeHeaders(final HttpServletRequest request, final HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "http://127.0.0.1:9000");
        response.addHeader("Access-Control-Allow-Credentials", "true");
        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            // OPTIONS is issued before Cross-origin request to get what we support.
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Origin,Content-Type, Accept");
        }
    }

}

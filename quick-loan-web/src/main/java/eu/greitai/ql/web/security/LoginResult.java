package eu.greitai.ql.web.security;

/**
 * 
 * Defines login result that should be returned to javascript.
 * 
 * @author gytis
 */
public class LoginResult {

    private Status status;

    public LoginResult(final Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    /**
     * 
     * Indicates if login succeeded or not.
     * 
     * @author gytis
     */
    public enum Status {
        OK,
        FAILED
    }
}

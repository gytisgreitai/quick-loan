package eu.greitai.ql.web.config;

import java.io.FileInputStream;
import java.sql.DriverManager;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

/**
 * 
 * Provides {@link DataSource} for db connection.
 * 
 * FIXME: load url from properties
 * 
 * @author gytis
 */
@Configuration
public class DataSourceProvider {

    @Bean
    public DataSource dataSource() {
        try {
            Properties props = new Properties();
            props.load(new FileInputStream("../conf/db.properties"));

            return new LazyConnectionDataSourceProxy(
                    new SimpleDriverDataSource(
                            DriverManager.getDriver("jdbc:postgresql://localhost:5432/ql"),
                            "jdbc:postgresql://localhost:5432/ql",
                            "ql",
                            "ql"));
        } catch (Exception e) {
            throw new IllegalStateException("Failed loading database properties ../conf/db.properties configuration", e);
        }
    }

}

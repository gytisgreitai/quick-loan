package eu.greitai.ql.web.dto;

import java.math.BigDecimal;

public class LoanCalculateRequest {

    private int duration;

    private BigDecimal amount;

    public int getDuration() {
        return duration;
    }

    public void setDuration(final int duration) {
        this.duration = duration;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

}

package eu.greitai.ql.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import eu.greitai.ql.web.security.LoginResult.Status;

/**
 * 
 * Invoked when authentication succeeds - instead of redirect, we return JSON result.
 * 
 * @author gytis
 */
@Component
public class AuthSuccessHandler implements AuthenticationSuccessHandler {


    @Override
    public void onAuthenticationSuccess(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final Authentication authentication) throws IOException, ServletException {
        // LoggedInUser acc = (LoggedInUser) authentication.getPrincipal();
        response.getWriter().write(new Gson().toJson(new LoginResult(Status.OK)));
    }

}

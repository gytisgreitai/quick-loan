package eu.greitai.ql.web.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * 
 * Marker class that is required for java based config spring security to work.
 * 
 * @author gytis
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}

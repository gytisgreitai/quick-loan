var app = angular.module('ql');
 
app.controller('MainController', ['$scope', '$http','settings',
  function MainController($scope, $http, settings) {
	$scope.settings = {
			amount : {
				min:50,
				max:1000,
				step:50
			},
			duration : {
				min:10,
				max:60,
				step:1
			}
	};
	$scope.amount = 50;
	$scope.duration = 30;
  }]);

var app = angular.module('ql');

app.config([ '$routeProvider', function($routeProvider) {
	$routeProvider
	.when('/main', {
		templateUrl : '/templates/main.html',
		controller : 'MainController',
		resolve: {
            settings: function (settingsService) {
                return settingsService.get();
            }
        }
	}).when('/my-loans', {
		templateUrl : '/templates/myloans.html',
		controller : 'MyLoansController'
	}).otherwise({
		redirectTo : '/main'
	});
} ]);

app.run(['$rootScope', function($root) {
	$root.$on('$routeChangeStart', function(e, curr, prev) { 
		if (curr.$$route && curr.$$route.resolve) {
			$root.loadingView = true;
		}
	});
	$root.$on('$routeChangeSuccess', function(e, curr, prev) { 
		$root.loadingView = false;
	});
}]);
var app = angular.module('ql');
/**
 * Settings service - hold global app settings.
 */
app.service('settingsService', ['$http', function ($http) {
    return { 
    	/**
    	 * Settings after retrieval from backend will be store here
    	 */
        config : null,
        /**
         * Gets system settings. 
         * @return system settings, or a promise if settings were not available and will be retrieved from backend.
         * 
         */
        get : function() {
        	var config = this.config;
        	
        	//no data, lets check the server side.
        	if(config == null) {
        		 var promise = $http({
                     url: '/api/settings',
                     method: "GET",
                     params: {}
                 });
        		 promise.success(function (res) {
        			 config = res;
        		 });
        		 return promise;
        	}
        	return config;
        }
    };
}]);
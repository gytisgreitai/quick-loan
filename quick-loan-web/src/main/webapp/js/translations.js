var app = angular.module('ql');
app.config([ '$translateProvider', function($translateProvider) {
	$translateProvider.translations({
		header : 'Quick Loan Solutions',
		main_intro : 'Get a quick loan',
		loan_header: 'Choose amount and number of days',
		main_marketing_text: 'No cash? Have no fear, the force is with you. Take out a loan',
		menu_my_loans: 'My Loans',
		menu_home: 'Home',
		loading: 'Please wait, loading',
		loading_header: 'Loading awesomeness. Please stand by',
		loan_duration: 'Days:',
		loan_amount: 'Dollars:',
		loan_header_amount: 'You take:',
		loan_header_duration: 'And after:',
	    loan_header_return: 'You return:',
	    loan_header_our_part: 'Our part:'
	});
}]);
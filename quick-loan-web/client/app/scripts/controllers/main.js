'use strict';
/* global accounting */
/*
Our home page - displays loan selection info
*/
angular.module('ql').controller('MainController', ['$scope', 'settings', '$http', '$location',
	function ($scope, settings, $http, $location) {
		// Set the initial slider settings
		$scope.settings = {
			amount : {
				min: settings.AMOUNT_MIN,
				max: settings.AMOUNT_MAX,
				step: 1
			},
			duration : {
				min: settings.DURATION_MIN,
				max: settings.DURATION_MAX,
				step: 1
			},
			// how much you have to pay per day for
			rate: settings.DAILY_INTEREST_RATE
		};
		$scope.interests = 0;
		$scope.total = 0;

		//set values for initial duration and amount.
		$scope.amount = parseFloat(settings.AMOUNT_INITIAL);
		$scope.duration = parseInt(settings.DURATION_INITIAL, 10);

		//watch amount and duration for changes.
		$scope.$watch('amount', function() {
			recalculateTotals();
		});

		$scope.$watch('duration', function() {
			recalculateTotals();
		});

		$scope.apply = function() {
		  $location.path('/loan/apply').search({amount:$scope.amount, duration:$scope.duration});
		};

		// calculate total loan amount and interest rates.
		function recalculateTotals() {
			$scope.interests = accounting.toFixed($scope.settings.rate * $scope.amount * $scope.duration, 2);
			$scope.total = accounting.toFixed(parseFloat($scope.amount) + parseFloat($scope.interests),2);
		}
	}]);

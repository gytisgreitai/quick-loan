'use strict';
/*
Handles final step of loan application - where user agrees to the loan terms, and submits his applicaiton
*/
angular.module('ql')
  .controller('LoanApplyController', ['$scope', 'loanService', '$location', function ($scope, loanService, $location) {
    var params = $location.search();
    // Holds calculated loan data - amount, total amount and return date.
    loanService.calculate(params.duration, params.amount).then(function(data){
      $scope.loan = data;
    });

    //flag to indicate if loan application failed
    $scope.applicationFailed = false;

    /*
    confirms and applies for a loan
    */
    $scope.confirm = function() {
      $scope.applicationFailed = false;
      loanService.apply($scope.loan.amount, $scope.loan.duration, $scope.loan.total).then(function(result){
        //check result
        if (result.status === 'OK') {
          // if ok, redirect to my loans page.
          $location.path('/loan/my').search({applied:true});
        } else {
          $scope.applicationFailed = true;
        }
      });
    };
  }]);

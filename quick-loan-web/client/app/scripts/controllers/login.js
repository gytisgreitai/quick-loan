'use strict';
/*
Handles login/signup scenario. Login form is currenly shown in modal
*/
angular.module('ql')
  .controller('LoginController',
    ['$scope', '$modalInstance', 'loginService', '$location',
    function ($scope, $modalInstance, loginService, $location) {
    //Indicates if we are dealing with existing customer, and if password repeat field should not visible
    $scope.existingCustomer = true;

    //indicates that login has failed.
    $scope.loginOk = null;

    //holds our user model mapped to the login form.
    $scope.user = {};

    $scope.$watch('loginOk', function(loginOk) {
      if(loginOk === true) {
        console.log('closing modal');
        //lets close this modal, and everything shoud proceed.
        //$modalInstance.close();
      }
    });

    // processes login form.
    $scope.login = function() {
      loginService.login($scope.user.email, $scope.user.password).then(function(data) {
        $scope.loginOk = data;
      });
    };
    // dismisses login window. Since we had this login window for a reason, lets redirect user to the public front page.
    // because he does not want to login.
    $scope.dismiss = function() {
      $modalInstance.dismiss('cancel');
      $location.path('/');
    };
  }]);

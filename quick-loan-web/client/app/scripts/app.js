'use strict';

angular.module('ql', [
	'config',
	'ngCookies',
	'ngResource',
	'ngSanitize',
	'pascalprecht.translate',
	'ngRoute',
  'uiSlider',
  'http-auth-interceptor',
  'ui.bootstrap',
  'ui.router'
])
.config([ '$routeProvider', function($routeProvider) {
	$routeProvider
	.when('/main', {
		templateUrl : '/views/main.html',
		controller : 'MainController',
		resolve: {
			settings: function (settingsService) {
				return settingsService.get();
			}
		}
	}).when('/loan/my', {
		templateUrl : '/views/myloans.html',
		controller : 'MyLoansController'
	}).when('/loan/apply', {
    templateUrl : '/views/apply.html',
    controller : 'LoanApplyController'
  }).otherwise({
		redirectTo : '/main'
	});
}])
.run(['$rootScope', function($root) {
	$root.$on('$routeChangeStart', function(e, curr) {
		if (curr.$$route && curr.$$route.resolve) {
			$root.loadingView = true;
		}
	});
	$root.$on('$routeChangeSuccess', function() {
		$root.loadingView = false;
	});
}])
// We use http-auth interceptors to intercept 401 requests, and queue them until user logins in.
// Library in use : https://github.com/witoldsz/angular-http-auth
// This quick fix is due to angular 1.2 changes the way interceptors are wired.
// @see https://github.com/witoldsz/angular-http-auth/issues/20
.config(['$httpProvider', function($httpProvider) {
  $httpProvider.defaults.withCredentials = true;
  var interceptor = function($rootScope, $q, httpBuffer) {
    return {
      'responseError': function(response) {
        if (response.status === 401 && !response.config.ignoreAuthModule) {
          var deferred = $q.defer();
          httpBuffer.append(response.config, deferred);
          $rootScope.$broadcast('event:auth-loginRequired');
          return deferred.promise;
        }
            // otherwise, default behaviour
        return $q.reject(response);
      }
    };
  };
  $httpProvider.interceptors.push(interceptor);
}]);

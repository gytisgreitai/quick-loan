'use strict';
/*
Handles login related logic with the backend
authService is required so that we could mark current state as loginConfirmed
*/
angular.module('ql')
  .service('loginService', ['$http','API','authService', function ($http, API, authService) {
    return {
      /*
      Tries to login user via backend with given userName and password
      */
      login: function(userName,password) {
          return $http.post(API + '/login', {userName:userName, password:password}
            ).then(function(result) {
              // if we've got our result ok, call auth service to mark user as logged in
              // this is needed if we have some pending api requests which should be resumed
              if(result.data.status === 'OK') {
                authService.loginConfirmed();
                return true;
              }
              return false;
            });
        }
    };
  }]);

'use strict';
/*
Our settings service that retrieves application runtime settings from our api
*/
angular.module('ql').service('settingsService', ['$http','API', function ($http,API) {
    return {
        config : null,
        /*
        Gets configuration. This either returns a promise, if no configuration is present
        or configuration if one already exists
        */
        get : function() {
            var service = this;

            //no data, lets check the server side.
            if(service.config === null) {
              return $http({
                url: API + '/settings',
                method: 'GET',
                params: {}
              }).then(function (data) {
                service.config = data.data;
                return data.data;
              });
            }
            return service.config;
          }
      };
  }]);

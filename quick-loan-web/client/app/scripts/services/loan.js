'use strict';

angular.module('ql')
  .service('loanService', ['$http','API', function ($http, API) {
    return {
      /*
      @description
      Calculates loan for the given params

      @param {int} duration - loan duration in days
      @param {bigdecimal} amount - loan amount
      @returns {promise} A promise with calculated loan data which includes total amount, date, etc.
      */
      calculate: function(duration, amount) {
        return $http.post(API + '/private/calculate', {duration:duration, amount:amount}).then(function(result) {
          return result.data;
        });
      },

      /*
      @description
      applies for a  loan for the given params and currently logged in user.

      @param {bigdecimal} amount - loan amount
      @param {int} duration - loan duration in days
      @param {bigdecimal} seenTotal - amount that user has seen that he will have to payback
      @returns {promise} A promise indicating if application succeeded (status === 'OK') or failed.
      */
      apply: function (amount, duration, seenTotal) {
        return {status:'OK'};
      }
    };
  }]);

'use strict';
/*
Angular-translate bootstraper, and our transalation file. If needed, move this to async loading
Currently only single language is implemented
*/
/* jshint -W106 */
angular.module('ql').config([ '$translateProvider', function($translateProvider) {
	$translateProvider.translations({
		header: 'Quick Loan Solutions',
    cancel: 'cancel',
		main_intro: 'Get a quick loan',
		loan_header: 'Choose amount and number of days',
		main_marketing_text: 'No cash? Have no fear, the force is with you. Take out a loan',
		menu_my_loans: 'My Loans',
		menu_home: 'Home',
		loading: 'Please wait, loading',
		loading_header: 'Loading awesomeness. Please stand by',
		loan_duration: 'Days:',
		loan_amount: 'Dollars:',
		loan_header_amount: 'You take:',
		loan_header_duration: 'And after:',
		loan_header_return: 'You return:',
		loan_header_our_part: 'Our part:',
    loan_apply: 'I want it!',
    login_form_title: 'Ups, login required',
    login_form_email: 'Email address',
    login_form_password: 'Password',
    login_form_password_repeat: 'Password repeat',
    login_form_login: 'Login',
    login_form_first_time: 'I\'ve already done business with you',
    login_form_email_req: 'You have to give us your email',
    login_form_email_valid: 'Doesn\'t look like valid email',
    login_form_pass_req: 'We need your password to log you in',
    login_form_pass_not_match: 'Passwords don\'t match',
    loan_apply_header: 'Confirm loan application',
    loan_apply_amount: 'Money you get:',
    loan_apply_total: 'Total amount you pay back:',
    loan_apply_return_date: 'Loan return date:',
    loan_apply_ok: 'I agree and take the loan',
    loan_apply_please_wait: 'Please wait, calculation in progress',
    loan_apply_failed: 'We\'re sorry, but our skilled profesionals using a highly ' +
                        'scientific algorithm have determined that you cannot apply for this loan'
	});
}]);
/* jshint +W106 */

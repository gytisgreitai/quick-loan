'use strict';

/*
Diretive that enables us to check equality of too fields
define directive as value-match, the use value-match-to to specify model attribute to watch
and if you require conditional validation use value-match-enabled to specify when this matcher should be enabled
*/
angular.module('ql')
.directive('valueMatch', [function () {
    return {
        restrict: 'A',
        scope: { isEnabled:'@valueMatchEnabled', compareTo: '@valueMatchTo' },
        require: 'ngModel',
        link: function (scope, elem , attrs,control) {
            var checker = function () {
                return scope.isEnabled === 'false' || control.$modelValue === scope.compareTo;
              };
            scope.$watch(checker, function (n) {
                //set the form control to valid if both
                //values are the same, else invalid
                control.$setValidity('valueMatch', n);
              });
          }
      };
  }]);

'use strict';
/*
Login form directive, that should sit on index page, and show modal popup when required
*/
angular.module('ql')
  .directive('loginForm', ['$modal', function ($modal) {
    return {
      template: '<div></div>',
      link: function postLink(scope) {
        // currently only way to login is when 401 requests are received from api
        // when 401 is received, and event is broadcated. Lets catch it, and show modal.
        scope.$on('event:auth-loginRequired', function() {
          $modal.open({
              templateUrl: 'views/modals/loginForm.html',
              backrop: 'static',
              controller: 'LoginController',
            });
        });
      }
    };
  }]);

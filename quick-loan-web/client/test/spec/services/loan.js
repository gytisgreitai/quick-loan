'use strict';

describe('Service: Loan', function () {

  // load the service's module
  beforeEach(module('QuickLoanApp'));

  // instantiate service
  var Loan;
  beforeEach(inject(function(_Loan_) {
    Loan = _Loan_;
  }));

  it('should do something', function () {
    expect(!!Loan).toBe(true);
  });

});

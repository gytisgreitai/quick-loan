'use strict';

describe('When i call login method()', function () {

  // load the service's module
  beforeEach(module('ql'));
  beforeEach(angular.mock.module('qlMock'));

  // instantiate service
  var loginService,
  authService,
  $httpBackend;
  beforeEach(function() {
    // replace auth service with a mock.
    // this seems kind of dirty... is there a better way?
    module(function($provide, $injector){
      authService = $injector.get('authServiceMockProvider').$get();
      $provide.value('authService', authService);
    });

    //actually get the loginService
    /*jshint camelcase: false */
    inject(function(_loginService_, _$httpBackend_) {
      loginService = _loginService_;
      $httpBackend =_$httpBackend_;
    });

    //http auth module method, that should be call only on success scenarios
    spyOn(authService, 'loginConfirmed').andCallThrough();
  });

  it('it should post to backened via /login method', function () {
    $httpBackend
    .expectPOST(ql.mock.API + '/login', {userName:'user', password:'pwd'})
    .respond({status: 'OK'});

    loginService.login('user', 'pwd');
    $httpBackend.flush();
  });

  it('it should return true when backend says ok and call auth service', function () {
    $httpBackend
    .expectPOST(ql.mock.API + '/login', {userName:'super', password:'longerPassword'})
    .respond({status: 'OK'});

    var promiseResult;
    loginService.login('super', 'longerPassword').then(function (result) {
      promiseResult = result;
    });
    //flush it
    $httpBackend.flush();

    //verify
    expect(promiseResult).toBe(true);
    expect(authService.loginConfirmed.callCount).toEqual(1);
  });

  it('it should return false when backend status is not ok', function () {
    $httpBackend
    .expectPOST(ql.mock.API + '/login', {userName:'super', password:'longerPassword'})
    .respond({status: 'FAILED'});
    var promiseResult;

    loginService.login('super', 'longerPassword').then(function (result) {
      promiseResult = result;
    });

    $httpBackend.flush();
    //verify
    expect(promiseResult).toBe(false);
    expect(authService.loginConfirmed.callCount).toEqual(0);
  });

});

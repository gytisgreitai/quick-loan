'use strict';

describe('Loan controlller tests', function () {

  // load the controller's module
  beforeEach(module('ql'));
  beforeEach(angular.mock.module('qlMock'));

  var loanApplyController,
    loanApplyServiceMock,
    scope;

  // Initialize the controller and a mock scope
  /*jshint camelcase: false */
  beforeEach(inject(function ($controller, $rootScope, _loanApplyServiceMock_) {
    scope = $rootScope.$new();
    loanApplyServiceMock =  _loanApplyServiceMock_;
  }));

  describe('When i load up loan controller', function() {
    beforeEach(inject(function ($controller) {
        loanApplyController = $controller('LoanApplyController', {
          $scope: scope,
          loanApplyService: loanApplyServiceMock,
          $location: function() {}
        });
      }));
    it('should attach a list of awesomeThings to the scope', function () {
      expect(scope.awesomeThings.length).toBe(3);
    });
  });
});

'use strict';

describe('When i invoke login controller login method', function () {
  // load the controller's module
  beforeEach(module('ql'));

  beforeEach(angular.mock.module('qlMock'));

  var loginController,
    loginServiceMock,
    modalInstanceMock,
    scope;

  // Initialize the controller and a mock scope
  /*jshint camelcase: false */
  beforeEach(inject(function ($controller, $rootScope, _loginServiceMock_) {
    scope = $rootScope.$new();
    loginServiceMock =  _loginServiceMock_;
    //setup simple mocks.
    modalInstanceMock = { close : function() {}};
    loginController = $controller('LoginController', {
      $scope: scope,
      loginService: loginServiceMock,
      $modalInstance: modalInstanceMock
    });
    spyOn(modalInstanceMock, 'close').andCallThrough();
  }));

  it('should close modal instance on login success', function () {
    //set up expectation
    spyOn(loginServiceMock, 'login').andReturn(true);

    //invoke the login
    scope.login();

    //check that we have tried to close the modal and all is ok
    expect(scope.loginOk).toBe(true);
    expect(modalInstanceMock.close.callCount).toEqual(1);
  });

  it('should not close modal instance on login failure', function () {
    //set up expectations
    spyOn(loginServiceMock, 'login').andReturn(false);

    //invoke the login
    scope.login();

    //no calls to close
    expect(scope.loginOk).toBe(false);
    expect(modalInstanceMock.close.callCount).toEqual(0);
  });

  it('should pass user name and password variables to service', function () {
    //set up expectation
    spyOn(loginServiceMock, 'login');

    //setup model variables
    scope.user = {email:'hire', password:'me'};
    //invoke the login
    scope.login();

    //check for correct variables and call count
    expect(loginServiceMock.login).toHaveBeenCalledWith('hire', 'me');
    expect(loginServiceMock.login.callCount).toEqual(1);
  });
});

'use strict';
/* global accounting */
describe('Controller: MainController', function () {

  // load the controller's module
  beforeEach(module('ql'));

  var MainController,
    settings,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    settings = { AMOUNT_INITIAL: 50, DURATION_INITIAL: 1, DAILY_INTEREST_RATE: 50};
    MainController = $controller('MainController', {
      $scope: scope,
      settings: settings
    });
  }));

  it('Shouldd persist settings onto local varialbes', function () {
    expect(scope.amount).toBe(settings.AMOUNT_INITIAL);
    expect(scope.duration).toBe(settings.DURATION_INITIAL);
  });

  it('Should watch for amount and duration change and calculate interest rates', function () {
    scope.$apply(function() {
        scope.amount = 100;
        scope.duration = 10;
      });

    var interests = 100 * 10 * settings.DAILY_INTEREST_RATE;
    expect(scope.interests).toBe(accounting.toFixed(interests, 2));
    expect(scope.total).toBe(accounting.toFixed(100 + interests,2));
  });
});

'use strict';
// this is definition, tell jsnit to ignore this.
/* global
    -ql
*/
//lets define namespace under which our mocks will be available
var ql = {};
ql.mock = {};

//this is our API variable that is defined in grunt config.
ql.mock.API = 'http://localhost:8080/api';

//and create module that depends on angular
angular.module('qlMock', ['ng']);

'use strict';
//lets mock http auth  service, so it would be spied upon.
ql.mock.authServiceMockProvider = function() {
  this.$get = function() {
    var $service = {
      loginConfirmed: function() { }
    };
    return $service;
  };
};

//and register it.
angular.module('qlMock').provider({
  authServiceMock: ql.mock.authServiceMockProvider
});

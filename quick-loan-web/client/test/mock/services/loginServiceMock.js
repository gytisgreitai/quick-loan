'use strict';
//lets mock our login service, so it would be spied upon.
ql.mock.loginServiceMockProvider = function() {
  this.$get = function() {
    var $service = {
      login: function() { }
    };
    return $service;
  };
};

//and register it.
angular.module('qlMock').provider({
  loginServiceMock: ql.mock.loginServiceMockProvider
});

package eu.greitai.ql.repository.test;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.DateTime;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import eu.greitai.ql.model.Loan;
import eu.greitai.ql.model.Loan.Type;
import eu.greitai.ql.model.User;
import eu.greitai.ql.repository.config.RepositoryConfig;

/**
 * 
 * Base repository tests that access database and test repositories.
 * 
 * @author gytis
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        RepositoryConfig.class,
        RepositoryTestConfig.class
})
// marker only, note that we are using TEST database configuration see {@link RepositoryTestConfig}.
@ActiveProfiles("test")
@TransactionConfiguration(defaultRollback = true)
public abstract class BaseRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @PersistenceContext
    protected EntityManager entityManager;

    protected void flush() {
        entityManager.flush();
    }

    protected User prepareUser(final String email) {
        User user = new User();
        user.setActivatedOn(new DateTime());
        user.setEmail(email);
        user.setPassword(email + "pass");
        return user;
    }


    protected Loan prepareLoan(final BigDecimal total, final User user) {
        Loan loan = new Loan();
        loan.setAmount(BigDecimal.ONE);
        loan.setCurrent(true);
        loan.setInterests(BigDecimal.ONE);
        loan.setIssueDate(new DateTime());
        loan.setLoanGuid(UUID.randomUUID());
        loan.setReturnDate(new DateTime().plusMonths(1));
        loan.setTotal(total);
        loan.setType(Type.ORIGINAL);
        loan.setUser(user);
        return loan;
    }
}

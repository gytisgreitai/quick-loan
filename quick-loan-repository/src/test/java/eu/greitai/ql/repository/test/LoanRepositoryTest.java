package eu.greitai.ql.repository.test;

import java.math.BigDecimal;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import eu.greitai.ql.model.Loan;
import eu.greitai.ql.model.Loan.Type;
import eu.greitai.ql.model.User;
import eu.greitai.ql.repository.LoanRepository;
import eu.greitai.ql.repository.UserRepository;

/**
 * 
 * Tests for {@link Loan} access via {@link LoanRepository}
 * 
 * @author gytis
 */
public class LoanRepositoryTest extends BaseRepositoryTest {

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    public void canCreateLoan() {
        User user = prepareUser(new DateTime().getMillis() + "@example.com");
        userRepository.save(user);

        Loan loan = prepareLoan(BigDecimal.TEN, user);
        loanRepository.save(loan);
        flush();

        Loan retrieved = loanRepository.findOne(loan.getId());
        Assert.assertEquals("Loans shoud match", loan, retrieved);
    }

    /**
     * Save three loans with same guid, retrieve with user id and check that sort order is correct
     */
    @Test
    @Transactional
    public void canRetrieveLoanLoanByUserInCorrectOrder() {
        User user = prepareUser(new DateTime().getMillis() + "@example.com");
        userRepository.save(user);

        // first loan
        Loan loan = prepareLoan(BigDecimal.TEN, user);
        loan.setCurrent(false);
        // and then an extension
        Loan extension = prepareLoan(BigDecimal.TEN, user);
        extension.setCurrent(false);
        extension.setLoanGuid(loan.getLoanGuid());
        extension.setType(Type.EXTENSION);
        // and another extension
        Loan anotherExtension = prepareLoan(BigDecimal.TEN, user);
        anotherExtension.setCurrent(true);
        anotherExtension.setLoanGuid(loan.getLoanGuid());
        anotherExtension.setType(Type.EXTENSION);

        loanRepository.save(loan);
        loanRepository.save(extension);
        loanRepository.save(anotherExtension);
        flush();

        // retrieve and verify
        List<Loan> loans = loanRepository.findForUser(user.getId());
        Assert.assertEquals("Should have gotten three loans", 3, loans.size());
        Assert.assertEquals("First loan should be original loan", loan, loans.get(0));
        Assert.assertEquals("Second loan should be first extension", extension, loans.get(1));
        Assert.assertEquals("Third loan should be last extension", anotherExtension, loans.get(2));
    }

    @Test
    @Transactional
    public void canRetrieveLoanLoanByUser() {
        // create two users
        User user = prepareUser(new DateTime().getMillis() + "@example.com");
        userRepository.save(user);
        User secondUser = prepareUser(new DateTime().getMillis() + "@example.com");
        userRepository.save(secondUser);

        // and a loan for each of them.
        Loan loan = prepareLoan(BigDecimal.TEN, user);
        loanRepository.save(loan);
        Loan secondLoan = prepareLoan(BigDecimal.TEN, secondUser);
        loanRepository.save(secondLoan);

        flush();

        // check that for correct one is retrieved
        List<Loan> firstUserLoans = loanRepository.findForUser(user.getId());
        Assert.assertEquals("Should have gotten one loan", 1, firstUserLoans.size());
        Assert.assertEquals("User loans should match", loan, firstUserLoans.get(0));

        List<Loan> secondUserLoans = loanRepository.findForUser(secondUser.getId());
        Assert.assertEquals("Should have gotten one loan", 1, secondUserLoans.size());
        Assert.assertEquals("User loans should match", secondLoan, secondUserLoans.get(0));

    }


    @Test
    @Transactional
    public void canRetrieveCurrentLoanByGuid() {
        User user = prepareUser(new DateTime().getMillis() + "@example.com");
        userRepository.save(user);

        // first loan
        Loan loan = prepareLoan(BigDecimal.TEN, user);
        loan.setCurrent(false);
        // and then an extension
        Loan extension = prepareLoan(BigDecimal.TEN, user);
        extension.setCurrent(true);
        extension.setLoanGuid(loan.getLoanGuid());
        extension.setType(Type.EXTENSION);

        //save it
        loanRepository.save(loan);
        loanRepository.save(extension);
        flush();

        Loan retrieved = loanRepository.findCurrentByLoanGuid(loan.getLoanGuid());
        Assert.assertEquals("Expected to retrieve last extension when looking for current guid", extension, retrieved);

    }
}
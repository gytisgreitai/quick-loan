package eu.greitai.ql.repository.test;


import javax.persistence.PersistenceException;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import eu.greitai.ql.model.User;
import eu.greitai.ql.repository.UserRepository;

/**
 * 
 * Simple database access tests for {@link User} repository
 * 
 * @author gytis
 */
public class UserRepositoryTest extends BaseRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    public void canCreateUser() {
        User user = new User();
        user.setActivatedOn(new DateTime());
        user.setEmail(new DateTime().getMillis() + "@email");
        user.setPassword("yadada");

        userRepository.save(user);

        flush();

        User retrieved = userRepository.findOne(user.getId());
        Assert.assertEquals("Users should match", user, retrieved);
    }

    @Test(expected = PersistenceException.class)
    @Transactional
    public void cannotCreateUserWithSameEmail() {

        User user = prepareUser("some@example.com");
        userRepository.save(user);
        flush();

        User another = prepareUser("some@example.com");
        userRepository.save(another);
        flush();

    }

    @Test
    @Transactional
    public void canLookupUserByEmail() {
        User user = new User();
        user.setActivatedOn(new DateTime());
        user.setEmail(new DateTime().getMillis() + "@email");
        user.setPassword("yadada");

        userRepository.save(user);

        flush();

        User retrieved = userRepository.findByEmail(user.getEmail());
        Assert.assertEquals("Users should match", user, retrieved);
    }


}

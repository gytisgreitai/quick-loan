package eu.greitai.ql.repository.test;

import java.io.FileInputStream;
import java.sql.DriverManager;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

/**
 * 
 * Provides {@link DataSource} for our {@link RepositoryTestConfig} for testing purposes.
 * 
 * Note that we use a different DB than for development.
 * 
 * @author gytis
 */
@Configuration
@Profile("test")
public class RepositoryTestConfig {

    private static final Logger log = LoggerFactory.getLogger(RepositoryTestConfig.class);

    @Bean
    public DataSource dataSource() {
        try {
            Properties props = new Properties();
            props.load(new FileInputStream("../conf/db.test.properties"));

            log.info("db properties loading from {}", props);

            return new LazyConnectionDataSourceProxy(
                    new SimpleDriverDataSource(
                            DriverManager.getDriver(props.getProperty("url")),
                            props.getProperty("url"),
                            props.getProperty("username"),
                            props.getProperty("password")));
        } catch (Exception e) {
            throw new IllegalStateException("Failed loading database properties ../conf/db.test.properties configuration", e);
        }
    }

}

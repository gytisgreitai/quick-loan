package eu.greitai.ql.repository.test;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import eu.greitai.ql.model.Audit;
import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.repository.AuditRepository;

/**
 * 
 * Tests for {@link Audit} access via {@link AuditRepository}
 * 
 * @author gytis
 */
public class AuditRepositoryTest extends BaseRepositoryTest {

    @Autowired
    private AuditRepository auditRepository;

    @Test
    @Transactional
    public void canSaveAudit() {
        Audit audit = new Audit();
        audit.setDate(new DateTime());
        audit.setEvent(Event.LOAN_TAKEN);
        audit.setIpAddress("127.0.0.1");

        auditRepository.save(audit);

        flush();

        Audit retrieved = auditRepository.findOne(audit.getId());
        Assert.assertEquals("Expected retrieved audits to match", audit, retrieved);
    }

    @Test
    @Transactional
    public void canCorrectlyCountByIpAddressEventTypeAndPeriod() {
        DateTime start = new DateTime().minusMonths(2);
        DateTime end = new DateTime().minusMonths(1);
        String ip = "127.0.0.1";

        // first audit - should be included
        Audit audit = new Audit();
        audit.setDate(start.plusDays(5));
        audit.setEvent(Event.LOAN_TAKEN);
        audit.setIpAddress(ip);
        auditRepository.save(audit);

        //second audit - should also be included.
        audit = new Audit();
        audit.setDate(start.plusDays(10));
        audit.setEvent(Event.LOAN_TAKEN);
        audit.setIpAddress(ip);
        auditRepository.save(audit);

        // third audit - invalid date, should not be included
        audit = new Audit();
        audit.setDate(end.plusDays(10));
        audit.setEvent(Event.LOAN_TAKEN);
        audit.setIpAddress(ip);
        auditRepository.save(audit);

        // fourth audit - invalid event type, should not be included
        audit = new Audit();
        audit.setDate(start.plusDays(3));
        audit.setEvent(Event.LOAN_EXTENDED);
        audit.setIpAddress(ip);
        auditRepository.save(audit);

        // firth audit - another ip address, should not be included
        audit = new Audit();
        audit.setDate(start.plusDays(3));
        audit.setEvent(Event.LOAN_TAKEN);
        audit.setIpAddress("192.168.1.1");
        auditRepository.save(audit);

        flush();

        Long count =  auditRepository.countByEventTypeForIpPerPeriod( Event.LOAN_TAKEN, ip, start, end);
        Long expected = 2L;
        Assert.assertEquals("Expected to get count of only two audits", expected, count);

    }

}

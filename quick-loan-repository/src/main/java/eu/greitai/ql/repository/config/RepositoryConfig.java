package eu.greitai.ql.repository.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 
 * Transaction, Database, Jpa, Repository configuration to access database and manage transactions.
 * 
 * For this to work, you must also provide a DataSource - a way to connect to database.
 * 
 * @author gytis
 */
@Configuration
@EnableJpaRepositories(basePackages = "eu.greitai.ql.repository")
@EnableTransactionManagement
@Import({ JpaAuditConfig.class })
public class RepositoryConfig {

    private static final Logger log = LoggerFactory.getLogger(RepositoryConfig.class);

    /**
     * When including this config, this should be provided via @Bean Datasource datasource()
     */
    @Autowired
    private DataSource dataSource;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory);
        return txManager;

    }

    @Bean
    public JpaDialect createJpaDialect() {
        return new HibernateJpaDialect();
    }

    @Bean
    public JpaVendorAdapter createJpaVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        log.info("Creating local container entity manager factory bean");

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setJpaVendorAdapter(createJpaVendorAdapter());
        factoryBean.setPackagesToScan("eu.greitai.ql.model");
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaDialect(createJpaDialect());
        // see {@link JpaAuditConfig}
        factoryBean.setMappingResources("META-INF/audit-listener.xml");

        Properties props = new Properties();
        props.put("hibernate.hbm2ddl.auto", "validate");

        props.put("jadira.usertype.autoRegisterUserTypes", true);
        props.put("jadira.usertype.databaseZone", "jvm");
        props.put("jadira.usertype.javaZone", "jvm");

        factoryBean.setJpaProperties(props);

        log.info("Initialize entity manager factory");
        factoryBean.afterPropertiesSet();

        EntityManagerFactory factory = factoryBean.getObject();

        return factory;
    }
}

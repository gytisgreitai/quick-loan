package eu.greitai.ql.repository.audit;

import org.joda.time.DateTime;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.stereotype.Component;

/**
 * 
 * implementation of {@link DateTimeProvider} for audit purposes that provides current date and time.
 * 
 * @author gytis
 */
@Component("currentdateTimeProvider")
public class CurrentDateTimeProvider implements DateTimeProvider {

    @Override
    public DateTime getDateTime() {
        return DateTime.now();
    }
}

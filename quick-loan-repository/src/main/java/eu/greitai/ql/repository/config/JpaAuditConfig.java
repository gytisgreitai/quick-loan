package eu.greitai.ql.repository.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Defines JPA auditing configuration.
 * As of 2013.11.16 only XMl config is availabe. Java config is done in 1.5 JPA, but it is still in beta
 * see https://jira.springsource.org/browse/DATAJPA-265
 * 
 * @author gytis
 */
@Configuration
@ImportResource("classpath:META-INF/audit.xml")
@ComponentScan("eu.greitai.ql.repository.audit")
public class JpaAuditConfig {

}

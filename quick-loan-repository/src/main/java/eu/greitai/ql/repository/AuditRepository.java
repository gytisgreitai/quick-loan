package eu.greitai.ql.repository;

import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import eu.greitai.ql.model.Audit;
import eu.greitai.ql.model.Audit.Event;

/**
 * 
 * Database access for {@link Audit}.
 * 
 * @author gytis
 */
@Repository
public interface AuditRepository extends CrudRepository<Audit, Long> {


    /**
     * Gets count of given event by ip address per given period. Date range is inclusive.
     * 
     * @param ipAddress - ip address to get for
     * @param start - period start
     * @param end - period end
     * @return number of events.
     */
    @Query("select count(a.id) from Audit a where a.event = ?1  and a.ipAddress = ?2 and a.date >= ?3 and a.date <= ?4")
    Long countByEventTypeForIpPerPeriod(
            final Event event,
            final String ipAddress,
            final DateTime start,
            final DateTime end);

}

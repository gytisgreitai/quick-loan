package eu.greitai.ql.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import eu.greitai.ql.model.Loan;
import eu.greitai.ql.model.User;

/**
 * 
 * Database access for {@link Loan}.
 * 
 * @author gytis
 */
@Repository
public interface LoanRepository extends CrudRepository<Loan, Long> {

    /**
     * Finds all loan information for user ordered by loan guid (grouped by same loan) and by created date)
     * 
     * @param userId - id of {@link User#getId()}
     * @return list of user loans and extensions
     */
    @Query("select l from Loan l where l.user.id=?1 order by l.loanGuid, l.createdOn asc, l.current")
    List<Loan> findForUser(final Long userId);

    /**
     * Gets last loan data for a given loan.
     * 
     * @param loanGuid - loan group id
     * @return {@link Loan}
     */
    @Query("select l from Loan l where l.loanGuid=?1 and l.current = true")
    Loan findCurrentByLoanGuid(final UUID loanGuid);
}

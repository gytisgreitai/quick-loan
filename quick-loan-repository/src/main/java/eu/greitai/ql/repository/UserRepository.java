package eu.greitai.ql.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import eu.greitai.ql.model.User;

/**
 * 
 * Database access for {@link User}.
 * 
 * @author gytis
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * Looks up user by email.
     * 
     * @param email {@link User#getEmail()}
     * @return {@link User} or null if not found.
     */
    User findByEmail(String email);

}

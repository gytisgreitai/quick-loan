package eu.greitai.ql.security.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * Spring configuration module for quick-loan-security project. Include this in other projects where spring security is required.
 * 
 * @author gytis
 */
@Configuration
@ComponentScan("eu.greitai.ql.security")
public class SecurityConfig {

}

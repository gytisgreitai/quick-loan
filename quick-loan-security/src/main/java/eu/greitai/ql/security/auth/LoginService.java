package eu.greitai.ql.security.auth;

import eu.greitai.ql.model.User;

/**
 * 
 * Service for managing user login/logout.
 * 
 * @author gytis
 */
public interface LoginService {

    /**
     * Logs user in or creates, if one does not exist.
     * 
     * @param email - unique user email.
     * @param password - non hashed user password
     * @return null if user exists and invalid password was provided, or {@link User} if one was created.
     */
    User loginOrCreate(final String email, final CharSequence password);
}

package eu.greitai.ql.security.auth.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import eu.greitai.ql.model.User;
import eu.greitai.ql.security.auth.AuthenticatedUserToken;
import eu.greitai.ql.security.auth.LoggedInUser;
import eu.greitai.ql.security.auth.LoginService;

@Component
public class UsernamePasswordAuthProvider implements AuthenticationProvider {

    @Autowired
    private LoginService loginService;

    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
        User user = loginService.loginOrCreate(token.getName(), token.getCredentials().toString());
        if(user != null) {
            LoggedInUser loggedIn = new LoggedInUser();
            loggedIn.setUserId(user.getId());

            WebAuthenticationDetails details = (WebAuthenticationDetails)token.getDetails();
            loggedIn.getMetaData().put("ipAddress", details.getRemoteAddress());
            loggedIn.getMetaData().put("sessionId", details.getSessionId());
            return new AuthenticatedUserToken(loggedIn);
        }
        throw new AuthenticationCredentialsNotFoundException("Invalid password");
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.equals(authentication);
    }
}

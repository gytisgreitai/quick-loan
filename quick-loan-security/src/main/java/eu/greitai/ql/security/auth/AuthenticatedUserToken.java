package eu.greitai.ql.security.auth;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * 
 * Implementation of spring security {@link AbstractAuthenticationToken} that defines a user that has logged in.
 * 
 * @author gytis
 */
public class AuthenticatedUserToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 7264425168608012020L;

    private final LoggedInUser user;

    public AuthenticatedUserToken(final LoggedInUser user) {
        super(null);
        this.user = user;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null; // we will not store password here, no need.
    }

    @Override
    public Object getPrincipal() {
        return user;
    }

}

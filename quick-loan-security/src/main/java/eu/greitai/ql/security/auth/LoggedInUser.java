package eu.greitai.ql.security.auth;

import java.util.Map;

import com.google.common.collect.Maps;

import eu.greitai.ql.security.SecurityContext;

/**
 * 
 * Implementation of {@link SecurityContext} that represents a logged in user with user id.
 * 
 * @author gytis
 */
public class LoggedInUser implements SecurityContext {

    private Long userId;

    private final Map<String, Object> metaData = Maps.newHashMap();

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public Map<String, Object> getMetaData() {
        return metaData;
    }

    public void setUserId(final Long userId) {
        this.userId = userId;
    }

}

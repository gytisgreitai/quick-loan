package eu.greitai.ql.security.auth.impl;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import eu.greitai.ql.model.User;
import eu.greitai.ql.repository.UserRepository;
import eu.greitai.ql.security.auth.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

    private static final Logger log = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    private final PasswordEncoder encoder = new BCryptPasswordEncoder(8);

    @Override
    public User loginOrCreate(final String email, final CharSequence password) {
        String convertedEmail = email.toLowerCase().trim();
        User user = userRepository.findByEmail(convertedEmail);
        if (user == null) {
            log.debug("User does not exist, creating new {}", convertedEmail);
            user = new User();
            user.setEmail(convertedEmail);
            user.setPassword(encoder.encode(password));
            user.setActivatedOn(new DateTime());
            userRepository.save(user);
        } else {
            if (!encoder.matches(password, user.getPassword())) {
                log.debug("Auth failed, password do not match {}", convertedEmail);
                return null;
            }
        }
        return user;
    }

}

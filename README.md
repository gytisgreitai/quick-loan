# Quick Loan App

This a demo app for technology showcase

* Spring XML-less web app acting purely as backend, no views etc.
* AngularJS for single page front end app
* Maven for building and dependcy management
* Yeoman, Grunt, Bower for Client side app
* Protractor and Selenium for front end integration testing.

## Setup guide

* Ensure you have JDK (1.7) and maven
* Clone it
* 
* mvn jetty:run
* Navigate to http://localhost:8080/

### DB setup
CREATE USER ql WITH PASSWORD 'ql';
CREATE DATABASE ql;
GRANT ALL PRIVILEGES ON DATABASE ql to ql;

CREATE USER ql_test WITH PASSWORD 'ql_test';
CREATE DATABASE ql_test;
GRANT ALL PRIVILEGES ON DATABASE ql_test to ql_test;

mvn liquibase:update -Ptest


##Runing tests
For test runs nodejs must be installed. Before begining ensure that you have nodejs and npm installed

#### Windows setup
* Install PowerShell (Windows Management Framework Core) from <http://support.microsoft.com/kb/968929>
* Excute in command prompt 

		@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('http://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%systemdrive%\chocolatey\bin 
* Install nodejs : 

		cinst nodejs.install

#### Mac os
* Install homebrew 

		ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"

* Install nodejs
		
		brew install npm

#### Ubuntu
* Install nodejs

		sudo apt-get update
		sudo apt-get install -y python-software-properties python g++ make
		sudo add-apt-repository -y ppa:chris-lea/node.js
		sudo apt-get update
		sudo apt-get install nodejs
		sudo npm install -g npm
### Install NPM packages
		sudo npm install -g grunt-cli
		sudo npm install -g grunt
		sudo npm install -g karma
		sudo npm install -g protractor
		sudo npm install -g phantomjs
		sudo npm install -g bower

### Run

		mvn verify
 


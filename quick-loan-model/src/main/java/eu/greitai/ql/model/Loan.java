package eu.greitai.ql.model;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

import eu.greitai.ql.model.extension.Auditable;

/**
 * 
 * Represents Loan and Loan extensions taken by user. Basically this is loan and its history
 * 
 * To get current only loans, one should group by loan_guid and get those that have current flag set to true
 * 
 * @author gytis
 */
@Entity
@Table(name = "loan")
public class Loan extends Auditable {

    @Id
    @SequenceGenerator(name = "seq_loan_id", sequenceName = "seq_loan_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_loan_id")
    @Column(name = "id", updatable = false)
    private Long id;

    /**
     * Loan id used for grouping loan events
     */
    @Column(name = "loan_guid", nullable = false)
    @org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID loanGuid;

    /**
     * User who took out a loan
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = true)
    private User user;

    /**
     * Amount that user has requested
     */
    @NotNull
    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    /**
     * Calculated interests for loan.
     */
    @NotNull
    @Column(name = "interests", nullable = false)
    private BigDecimal interests;

    /**
     * Total amount that user must return
     */
    @NotNull
    @Column(name = "total", nullable = false)
    private BigDecimal total;

    /**
     * Date loan was issued. For extensions this should stay the same.
     */
    @NotNull
    @Column(name = "issue_date", nullable = false)
    @org.hibernate.annotations.Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime issueDate;

    /**
     * When loan should be returned?
     */
    @NotNull
    @Future
    @Column(name = "return_date", nullable = false)
    @org.hibernate.annotations.Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime returnDate;

    /**
     * Update date, applicable for extensions.
     */
    @Column(name = "update_date", nullable = true)
    @org.hibernate.annotations.Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime updateDate;

    /**
     * Indicates if this is the current loan info for loan guid.
     */
    @Column(name = "current", nullable = false)
    private boolean current;

    /**
     * Type of loan entry
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private Type type;

    /**
     * Loan entry type
     * 
     * @author gytis
     */
    public enum Type {
        /**
         * Original loan taken
         */
        ORIGINAL,
        /**
         * Loan return date was extended.
         */
        EXTENSION
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public UUID getLoanGuid() {
        return loanGuid;
    }

    public void setLoanGuid(final UUID loanGuid) {
        this.loanGuid = loanGuid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getInterests() {
        return interests;
    }

    public void setInterests(final BigDecimal interests) {
        this.interests = interests;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(final BigDecimal total) {
        this.total = total;
    }

    public DateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(final DateTime issueDate) {
        this.issueDate = issueDate;
    }

    public DateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(final DateTime returnDate) {
        this.returnDate = returnDate;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(final DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(final boolean current) {
        this.current = current;
    }

    public Type getType() {
        return type;
    }

    public void setType(final Type type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((amount == null) ? 0 : amount.hashCode());
        result = prime * result + (current ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((interests == null) ? 0 : interests.hashCode());
        result = prime * result + ((issueDate == null) ? 0 : issueDate.hashCode());
        result = prime * result + ((loanGuid == null) ? 0 : loanGuid.hashCode());
        result = prime * result + ((returnDate == null) ? 0 : returnDate.hashCode());
        result = prime * result + ((total == null) ? 0 : total.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Loan other = (Loan) obj;
        if (amount == null) {
            if (other.amount != null) {
                return false;
            }
        } else if (!amount.equals(other.amount)) {
            return false;
        }
        if (current != other.current) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (interests == null) {
            if (other.interests != null) {
                return false;
            }
        } else if (!interests.equals(other.interests)) {
            return false;
        }
        if (issueDate == null) {
            if (other.issueDate != null) {
                return false;
            }
        } else if (!issueDate.equals(other.issueDate)) {
            return false;
        }
        if (loanGuid == null) {
            if (other.loanGuid != null) {
                return false;
            }
        } else if (!loanGuid.equals(other.loanGuid)) {
            return false;
        }
        if (returnDate == null) {
            if (other.returnDate != null) {
                return false;
            }
        } else if (!returnDate.equals(other.returnDate)) {
            return false;
        }
        if (total == null) {
            if (other.total != null) {
                return false;
            }
        } else if (!total.equals(other.total)) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        if (updateDate == null) {
            if (other.updateDate != null) {
                return false;
            }
        } else if (!updateDate.equals(other.updateDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Loan [id=").append(id)
        .append(", loanGuid=").append(loanGuid)
        .append(", amount=").append(amount)
        .append(", interests=").append(interests)
        .append(", total=").append(total)
        .append(", issueDate=").append(issueDate)
        .append(", returnDate=").append(returnDate)
        .append(", updateDate=").append(updateDate)
        .append(", current=").append(current)
        .append(", type=").append(type)
        .append("]");
        return builder.toString();
    }

}

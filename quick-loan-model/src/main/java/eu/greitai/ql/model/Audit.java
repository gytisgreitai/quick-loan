package eu.greitai.ql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

/**
 * 
 * Holds information about various system events
 * 
 * @author gytis
 */
@Entity
@Table(name = "audit")
public class Audit {

    @Id
    @SequenceGenerator(name = "seq_audit_id", sequenceName = "seq_audit_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_audit_id")
    @Column(name = "id", updatable = false)
    private Long id;

    /**
     * When did id happen?
     */
    @NotNull
    @Past
    @Column(name = "date", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime date;

    /**
     * What happened?
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "event", nullable = false)
    private Event event;

    /**
     * Id of user, if applicable.
     */
    @Column(name = "user_id", nullable = true)
    private Long userId;

    /**
     * Ip address, which initiated the action, if applicable
     */
    @Column(name = "ip_address", nullable = true)
    private String ipAddress;

    /**
     * Any additional meta data.
     */
    @Column(name = "meta_data", nullable = true)
    private String metaData;

    /**
     * 
     * Types of possible events.
     * 
     * @author gytis
     */
    public enum Event {
        /**
         * Loan was taken.
         */
        LOAN_TAKEN,
        /**
         * Loan was extended
         */
        LOAN_EXTENDED,
        /**
         * User has logged in.
         */
        LOGIN
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(final DateTime date) {
        this.date = date;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(final Event event) {
        this.event = event;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(final Long userId) {
        this.userId = userId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(final String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(final String metaData) {
        this.metaData = metaData;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((event == null) ? 0 : event.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
        result = prime * result + ((metaData == null) ? 0 : metaData.hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Audit other = (Audit) obj;
        if (date == null) {
            if (other.date != null) {
                return false;
            }
        } else if (!date.equals(other.date)) {
            return false;
        }
        if (event != other.event) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (ipAddress == null) {
            if (other.ipAddress != null) {
                return false;
            }
        } else if (!ipAddress.equals(other.ipAddress)) {
            return false;
        }
        if (metaData == null) {
            if (other.metaData != null) {
                return false;
            }
        } else if (!metaData.equals(other.metaData)) {
            return false;
        }
        if (userId == null) {
            if (other.userId != null) {
                return false;
            }
        } else if (!userId.equals(other.userId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Audit [id=").append(id)
        .append(", date=").append(date)
        .append(", event=").append(event)
        .append(", userId=").append(userId)
        .append(", ipAddress=").append(ipAddress)
        .append(", metaData=").append(metaData).append("]");
        return builder.toString();
    }

}

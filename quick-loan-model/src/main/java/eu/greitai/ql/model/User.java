package eu.greitai.ql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import eu.greitai.ql.model.extension.Auditable;

/**
 * 
 * Represents our user entity - users that can take out loans.
 * 
 * @author gytis
 */
@Entity
@Table(name = "user_")
public class User extends Auditable {

    @Id
    @SequenceGenerator(name = "seq_user_id", sequenceName = "seq_user_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user_id")
    @Column(name = "id", updatable = false)
    private Long id;

    /**
     * Unique email address.
     */
    @Email
    @Column(name = "email", nullable = false)
    private String email;

    /**
     * Hashed password
     */
    @NotBlank
    @Column(name = "password", nullable = false)
    private String password;

    /**
     * When was this user activated?
     */
    @Column(name = "activated_on", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime activatedOn;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public DateTime getActivatedOn() {
        return activatedOn;
    }

    public void setActivatedOn(final DateTime activatedOn) {
        this.activatedOn = activatedOn;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((activatedOn == null) ? 0 : activatedOn.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (activatedOn == null) {
            if (other.activatedOn != null) {
                return false;
            }
        } else if (!activatedOn.equals(other.activatedOn)) {
            return false;
        }
        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("User [id=").append(id)
        .append(", email=").append(email)
        .append(", password=").append(password != null ? "************" : "")
        .append(", activatedOn=").append(activatedOn).append("]");
        return builder.toString();
    }

}

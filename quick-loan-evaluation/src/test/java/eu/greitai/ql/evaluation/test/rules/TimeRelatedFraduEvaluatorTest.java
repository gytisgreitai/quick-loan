package eu.greitai.ql.evaluation.test.rules;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import eu.greitai.ql.evaluation.LoanEvaluation;
import eu.greitai.ql.evaluation.rules.TimeRelatedFraudEvaluator;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.settings.LoanSettings;

/**
 * 
 * Tests for {@link TimeRelatedFraudEvaluator}
 * 
 * @author gytis
 */
public class TimeRelatedFraduEvaluatorTest extends BaseEvaluatorTest {

    @Autowired
    private TimeRelatedFraudEvaluator evaluator;

    @Mock
    @Autowired
    private SettingsService settings;

    @Before
    public void before() {
        Mockito.reset(settings);
    }

    @Test
    public void nonMaxAmountIsIgnored() {
        // prepare data - date within bad range, and amount less than max.
        DateTime date = new DateTime().withTime(0, 0, 30, 999);
        LoanEvaluation evaluation = EvalGenerator.createForAmount("4.56");
        evaluation.setDate(date);

        //expect call to settings
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MAX)).thenReturn(BigDecimal.TEN);

        // execute
        evaluation = evaluator.evaluate(evaluation);

        Assert.assertTrue("Should have passed evaluation with less than max", evaluation.accepted());
        Assert.assertNull("There should be no errors", evaluation.getErrors());
        Mockito.verify(settings).getAsDecimal(LoanSettings.AMOUNT_MAX);
    }

    @Test
    public void maxAmountIsNotPassedWhenInTimeRange() {
        // prepare data - date within bad range, and amount equal to max.
        DateTime date = new DateTime().withTime(0, 0, 30, 999);
        LoanEvaluation evaluation = EvalGenerator.createForAmount("10.00");
        evaluation.setDate(date);

        // expect call to settings
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MAX)).thenReturn(BigDecimal.TEN);

        // execute
        evaluation = evaluator.evaluate(evaluation);

        Assert.assertFalse("Should have failed evaluation with max mount", evaluation.accepted());
        Assert.assertTrue("There should be an MAX_AMOUNT_RISK_TIME error", evaluation.getErrors().contains("MAX_AMOUNT_RISK_TIME"));
        Mockito.verify(settings).getAsDecimal(LoanSettings.AMOUNT_MAX);
    }

    @Test
    public void maxAmountIsIgnoredWhenNotInTimeRange() {
        // prepare data - date within good range, and amount equal to max.
        DateTime date = new DateTime().withTime(0, 2, 0, 0);
        LoanEvaluation evaluation = EvalGenerator.createForAmount("10.00");
        evaluation.setDate(date);

        // expect call to settings
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MAX)).thenReturn(BigDecimal.TEN);

        // execute
        evaluation = evaluator.evaluate(evaluation);

        Assert.assertTrue("Should have passed evaluation with max within other time range", evaluation.accepted());
        Assert.assertNull("There should be no errors", evaluation.getErrors());
        Mockito.verify(settings).getAsDecimal(LoanSettings.AMOUNT_MAX);
    }

}

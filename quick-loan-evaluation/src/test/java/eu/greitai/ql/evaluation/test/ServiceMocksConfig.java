package eu.greitai.ql.evaluation.test;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.greitai.ql.services.AuditService;
import eu.greitai.ql.services.SettingsService;

@Configuration
public class ServiceMocksConfig {

    @Bean
    public SettingsService settingsService() {
        return Mockito.mock(SettingsService.class);
    }

    @Bean
    public AuditService auditService() {
        return Mockito.mock(AuditService.class);
    }

}

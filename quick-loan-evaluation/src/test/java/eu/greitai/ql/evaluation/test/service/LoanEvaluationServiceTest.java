package eu.greitai.ql.evaluation.test.service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.dto.LoanEvaluationResult;
import eu.greitai.ql.dto.LoanEvaluationResult.Result;
import eu.greitai.ql.evaluation.LoanEvaluator;
import eu.greitai.ql.evaluation.rules.LoanRangeEvaluator;
import eu.greitai.ql.evaluation.test.ServiceMocksConfig;
import eu.greitai.ql.evaluation.test.TestSecurityContext;
import eu.greitai.ql.evaluation.test.service.LoanEvaluationServiceTest.EvaluatorConfig;
import eu.greitai.ql.services.LoanEvaluationService;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.settings.LoanSettings;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceMocksConfig.class, EvaluatorConfig.class })
public class LoanEvaluationServiceTest {

    @Configuration
    @ComponentScan("eu.greitai.ql.evaluation")
    public static class EvaluatorConfig {}

    @Mock
    @Autowired
    private SettingsService settings;

    @Autowired
    private LoanEvaluationService service;

    @Autowired
    private List<LoanEvaluator> evaluators;

    @PostConstruct
    public void init() {
        Collections.sort(evaluators, AnnotationAwareOrderComparator.INSTANCE);
    }

    @Before
    public void before() {
        Mockito.reset(settings);
    }

    @Test(expected = NullPointerException.class)
    public void cannotPassInNullContext() {
        service.evaluate(null, new LoanCalculation());
    }

    @Test(expected = NullPointerException.class)
    public void cannotPassInNullLoanCalculation() {
        service.evaluate(new TestSecurityContext(), null);
    }

    @Test
    public void nullAmountIsRejectedFirst() {
        LoanCalculation loan = new LoanCalculation();
        loan.setAmount(null);
        LoanEvaluationResult result = service.evaluate(new TestSecurityContext(), loan);

        // we should fail and should have NO_LOAN_AMOUNT error
        Assert.assertEquals("Expected loan result to be rejected", Result.RISK_TOO_HIGH, result.getResult());
        Assert.assertEquals("Expected to have only single error", 1, result.getErrors().size());
        Assert.assertTrue("Exepcted to contain NO_LOAN_AMOUNT error ", result.getErrors().contains("NO_LOAN_AMOUNT"));

        Mockito.verifyNoMoreInteractions(settings);
    }

    @Test
    public void noMoreEvaluatorsAreCalledIfFirstFails() {
        LoanCalculation loan = new LoanCalculation();
        // set amount more than max - this should fail on first evaluator
        loan.setAmount(new BigDecimal("100"));

        // mock the now date - this should fail on second evaluator
        DateTimeUtils.setCurrentMillisFixed(new DateTime().withTime(0, 0, 1, 999).getMillis());

        // setup expectations
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MIN)).thenReturn(BigDecimal.ONE);
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MAX)).thenReturn(BigDecimal.TEN);

        LoanEvaluationResult result = service.evaluate(new TestSecurityContext(), loan);

        // we should fail and should have NOT_WITHIN_RANGE error
        Assert.assertEquals("Expected loan result to be rejected", Result.RISK_TOO_HIGH, result.getResult());
        Assert.assertEquals("Expected to have only single error", 1, result.getErrors().size());
        Assert.assertTrue("Exepcted to contain NOT_WITHIN_RANGE error ", result.getErrors().contains("NOT_WITHIN_RANGE"));

        Mockito.verify(settings).getAsDecimal(LoanSettings.AMOUNT_MIN);
        Mockito.verify(settings).getAsDecimal(LoanSettings.AMOUNT_MAX);
        Mockito.verifyNoMoreInteractions(settings);
    }

    @Test
    public void timeRelatedFraudEvaluatorIsCalled() {
        LoanCalculation loan = new LoanCalculation();
        loan.setAmount(BigDecimal.TEN);

        // mock the now date
        DateTimeUtils.setCurrentMillisFixed(new DateTime().withTime(0, 0, 1, 999).getMillis());

        // setup expectations
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MIN)).thenReturn(BigDecimal.ONE);
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MAX)).thenReturn(BigDecimal.TEN);

        LoanEvaluationResult result = service.evaluate(new TestSecurityContext(), loan);

        // we should fail and should have MAX_AMOUNT_RISK_TIME error
        Assert.assertEquals("Expected loan result to be rejected", Result.RISK_TOO_HIGH, result.getResult());
        Assert.assertEquals("Expected to have only single error", 1, result.getErrors().size());
        Assert.assertTrue("Exepcted to contain MAX_AMOUNT_RISK_TIME error ", result.getErrors().contains("MAX_AMOUNT_RISK_TIME"));

        Mockito.verify(settings).getAsDecimal(LoanSettings.AMOUNT_MIN);
        Mockito.verify(settings, Mockito.times(2)).getAsDecimal(LoanSettings.AMOUNT_MAX);
        Mockito.verifyNoMoreInteractions(settings);
    }

    @Test
    public void evaluatorsListHasCorrectOrder() {
        Assert.assertEquals("Should have only two evaluators", 3, evaluators.size());
        Assert.assertEquals("First evaluator should be preconditioncheck", LoanRangeEvaluator.class, evaluators.get(0).getClass());
    }
}

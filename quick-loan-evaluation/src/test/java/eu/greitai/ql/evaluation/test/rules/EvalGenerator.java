package eu.greitai.ql.evaluation.test.rules;

import java.math.BigDecimal;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.evaluation.LoanEvaluation;
import eu.greitai.ql.evaluation.test.TestSecurityContext;

/**
 * 
 * Helper for creating {@link LoanEvaluation}
 * 
 * @author gytis
 */
public class EvalGenerator {

    public static LoanEvaluation createForAmount(final String amount) {
        LoanEvaluation eval = new LoanEvaluation();
        eval.setLoan(new LoanCalculation());
        eval.getLoan().setAmount(new BigDecimal(amount));
        eval.setContext(new TestSecurityContext());
        return eval;
    }

}

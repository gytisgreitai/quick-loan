package eu.greitai.ql.evaluation.test.rules;

import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.greitai.ql.evaluation.LoanEvaluator;
import eu.greitai.ql.evaluation.test.ServiceMocksConfig;
import eu.greitai.ql.evaluation.test.rules.BaseEvaluatorTest.EvaluatorsConfig;

/**
 * 
 * base class for running {@link LoanEvaluator}s tests.
 * 
 * @author gytis
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceMocksConfig.class, EvaluatorsConfig.class })
public abstract class BaseEvaluatorTest {

    @Configuration
    @ComponentScan("eu.greitai.ql.evaluation.rules")
    public static class EvaluatorsConfig {

    }
}

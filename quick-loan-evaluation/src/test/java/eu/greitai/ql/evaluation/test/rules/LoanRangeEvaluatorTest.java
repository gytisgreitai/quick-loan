package eu.greitai.ql.evaluation.test.rules;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.evaluation.LoanEvaluation;
import eu.greitai.ql.evaluation.rules.LoanRangeEvaluator;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.settings.LoanSettings;

/**
 * Tests for {@link LoanRangeEvaluator}.
 * 
 * @author gytis
 */
public class LoanRangeEvaluatorTest extends BaseEvaluatorTest {

    @Autowired
    private LoanRangeEvaluator loanRangeEvaluator;

    @Mock
    @Autowired
    private SettingsService settings;

    @Before
    public void before() {
        Mockito.reset(settings);
    }

    @Test
    public void loanAmountCannotBeNull() {
        LoanEvaluation evaluation = new LoanEvaluation();
        evaluation.setLoan(new LoanCalculation());
        evaluation = loanRangeEvaluator.evaluate(evaluation);
        Assert.assertFalse("Should have rejected empty loan amount", evaluation.accepted());
        Assert.assertTrue("Error should contain empty loan amount", evaluation.getErrors().contains("NO_LOAN_AMOUNT"));
    }

    @Test
    public void settingsServiceIsCalledToRetrieveMinMaxAmmounts() {
        LoanEvaluation evaluation = EvalGenerator.createForAmount("100");
        expectCallsToSettingsServiceAndReturnMinMax(BigDecimal.ZERO, BigDecimal.TEN);

        // execute
        evaluation = loanRangeEvaluator.evaluate(evaluation);

        // verify that service was called.
        Mockito.verify(settings).getAsDecimal(LoanSettings.AMOUNT_MIN);
        Mockito.verify(settings).getAsDecimal(LoanSettings.AMOUNT_MAX);
    }


    @Test
    public void cannotAllowLessThanMinAmount() {
        LoanEvaluation evaluation = EvalGenerator.createForAmount("0.5");
        expectCallsToSettingsServiceAndReturnMinMax(BigDecimal.ONE, BigDecimal.TEN);

        evaluation = loanRangeEvaluator.evaluate(evaluation);

        Assert.assertFalse("Should have not allowed amount less than min", evaluation.accepted());
        Assert.assertTrue("Expected to find min amount error", evaluation.getErrors().contains("NOT_WITHIN_RANGE"));

    }

    @Test
    public void cannotAllowMoreThanMaxAmount() {
        LoanEvaluation evaluation = EvalGenerator.createForAmount("11");
        // expect calls
        expectCallsToSettingsServiceAndReturnMinMax(BigDecimal.ONE, BigDecimal.TEN);

        evaluation = loanRangeEvaluator.evaluate(evaluation);

        Assert.assertFalse("Should have not allowed amount more than max", evaluation.accepted());
        Assert.assertTrue("Expected to find max amount error", evaluation.getErrors().contains("NOT_WITHIN_RANGE"));

    }

    @Test
    public void amountEqualToMinIsAllowed() {
        LoanEvaluation evaluation = EvalGenerator.createForAmount("10.10");
        expectCallsToSettingsServiceAndReturnMinMax(new BigDecimal("10.1"), new BigDecimal("100"));

        evaluation = loanRangeEvaluator.evaluate(evaluation);

        Assert.assertTrue("Should have allowed amount equal to min", evaluation.accepted());
        Assert.assertNull("There should be no errors when amount is min", evaluation.getErrors());

    }

    @Test
    public void amountEqualToMaxIsAllowed() {
        LoanEvaluation evaluation = EvalGenerator.createForAmount("100.5");
        expectCallsToSettingsServiceAndReturnMinMax(BigDecimal.ONE, new BigDecimal("100.50"));

        evaluation = loanRangeEvaluator.evaluate(evaluation);

        Assert.assertTrue("Should have allowed amount equal to max", evaluation.accepted());
        Assert.assertNull("There should be no errors when amount is max", evaluation.getErrors());

    }

    @Test
    public void amountBetweenMinAndMaxIsAllowed() {
        LoanEvaluation evaluation = EvalGenerator.createForAmount("4.56");
        expectCallsToSettingsServiceAndReturnMinMax(BigDecimal.ONE, BigDecimal.TEN);

        evaluation = loanRangeEvaluator.evaluate(evaluation);

        Assert.assertTrue("Should have allowed amount between min and max", evaluation.accepted());
        Assert.assertNull("There should be no errors when amount is between min and max", evaluation.getErrors());

    }

    private void expectCallsToSettingsServiceAndReturnMinMax(final BigDecimal min, final BigDecimal max) {
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MIN)).thenReturn(min);
        Mockito.when(settings.getAsDecimal(LoanSettings.AMOUNT_MAX)).thenReturn(max);

    }

}

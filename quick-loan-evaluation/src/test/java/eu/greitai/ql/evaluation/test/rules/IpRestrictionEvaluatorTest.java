package eu.greitai.ql.evaluation.test.rules;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import eu.greitai.ql.evaluation.LoanEvaluation;
import eu.greitai.ql.evaluation.rules.IpRestrictionEvaluator;
import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.services.AuditService;

/**
 * 
 * Tests for {@link IpRestrictionEvaluator}.
 * 
 * @author gytis
 */
public class IpRestrictionEvaluatorTest extends BaseEvaluatorTest {

    @Autowired
    private IpRestrictionEvaluator evaluator;

    @Mock
    @Autowired
    private AuditService auditService;

    private DateTime start;

    private DateTime end;

    @Before
    public void before() {
        start = new DateTime().withTime(0, 0, 0, 0);
        end = new DateTime().withTime(23, 59, 59, 999);
        Mockito.reset(auditService);
    }

    @Test
    public void ipRestrictionDoesNotRejectForCountThatIsLowerThanThreshold() {
        Mockito.when(auditService.getCountByEventTypeForIpPerPeriod(
                Matchers.eq(Event.LOAN_TAKEN),
                Matchers.eq("127.0.0.1"),
                Matchers.eq(start),
                Matchers.eq(end))).thenReturn(1L);

        LoanEvaluation evaluation = EvalGenerator.createForAmount("10.00");
        evaluation = evaluator.evaluate(evaluation);

        Mockito.verify(auditService).getCountByEventTypeForIpPerPeriod(
                Matchers.eq(Event.LOAN_TAKEN),
                Matchers.eq("127.0.0.1"),
                Matchers.eq(start),
                Matchers.eq(end));

        Assert.assertTrue("Shoul have passed when ip count is less than limit", evaluation.accepted());

    }
}

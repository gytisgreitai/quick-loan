package eu.greitai.ql.evaluation.test;

import java.util.Map;

import com.google.common.collect.Maps;

import eu.greitai.ql.security.SecurityContext;

public class TestSecurityContext implements SecurityContext {

    @Override
    public Long getUserId() {
        return 1L;
    }

    @Override
    public Map<String, Object> getMetaData() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("ipAddress", "127.0.0.1");
        return map;
    }

}

package eu.greitai.ql.evaluation;

import org.springframework.core.Ordered;

/**
 * 
 * Represents single loan evaluator that should evaluate loan based on single criteria
 * 
 * When creating an evaluator be sure to order it with an extension from {@link Ordered}:
 * 0-100 generic pre-checks
 * 101-200 fraud prevention
 * 201-n user checks.
 * 
 * @author gytis
 */
public interface LoanEvaluator {

    /**
     * Evaluates given loan, and rejects or leaves object unchanged.
     * 
     * @param evaluation - loan evaluation object.
     * @return - {@link LoanEvaluation} rejected if evaluation failed, or unchanged.
     */
    public LoanEvaluation evaluate(LoanEvaluation evaluation);

}

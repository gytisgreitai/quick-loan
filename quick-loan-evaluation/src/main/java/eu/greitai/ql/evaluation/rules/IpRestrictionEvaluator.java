package eu.greitai.ql.evaluation.rules;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import eu.greitai.ql.evaluation.LoanEvaluation;
import eu.greitai.ql.evaluation.LoanEvaluator;
import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.services.AuditService;

/**
 * 
 * Checks that more than X loans can be take out per period for single IP address.
 * 
 * @author gytis
 */
@Component
public class IpRestrictionEvaluator implements LoanEvaluator, Ordered {

    private static final int LIMIT_PER_IP = 3;

    private static final Logger log = LoggerFactory.getLogger(IpRestrictionEvaluator.class);

    @Autowired
    private AuditService auditService;

    @Override
    public LoanEvaluation evaluate(final LoanEvaluation evaluation) {
        // range is current day.
        DateTime start = new DateTime().withTime(0, 0, 0, 0);
        DateTime end = new DateTime().withTime(23, 59, 59, 999);
        String ipAddress = evaluation.getContext().getMetaData().get("ipAddress").toString();

        log.debug("Will check loan count for ip {} ", ipAddress);
        Long count = auditService.getCountByEventTypeForIpPerPeriod(
                Event.LOAN_TAKEN,
                ipAddress,
                start,
                end);

        // if we reached the count, reject it.
        if (count >= LIMIT_PER_IP) {
            evaluation.reject("TOO_MANY_PER_IP");
        }
        return evaluation;
    }

    @Override
    public int getOrder() {
        return 102;
    }

}

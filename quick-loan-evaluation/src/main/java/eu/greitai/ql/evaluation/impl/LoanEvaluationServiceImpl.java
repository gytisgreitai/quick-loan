package eu.greitai.ql.evaluation.impl;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.dto.LoanEvaluationResult;
import eu.greitai.ql.dto.LoanEvaluationResult.Result;
import eu.greitai.ql.evaluation.LoanEvaluation;
import eu.greitai.ql.evaluation.LoanEvaluator;
import eu.greitai.ql.security.SecurityContext;
import eu.greitai.ql.services.LoanEvaluationService;

@Service
public class LoanEvaluationServiceImpl implements LoanEvaluationService {

    private static final Logger log = LoggerFactory.getLogger(LoanEvaluationServiceImpl.class);

    @Autowired
    private List<LoanEvaluator> evaluators;

    @PostConstruct
    public void init() {
        Collections.sort(evaluators, AnnotationAwareOrderComparator.INSTANCE);
    }

    @Override
    public LoanEvaluationResult evaluate(final SecurityContext context, final LoanCalculation loanApplication) {

        Preconditions.checkNotNull(context, "Cannot evaluate without context");
        Preconditions.checkNotNull(loanApplication, "Cannot evaluate without loan data");
        log.debug("Will evaluate loan appliation {} with context {}", loanApplication, context);
        // create object for passing around.
        LoanEvaluation evaluation = new LoanEvaluation();
        evaluation.setContext(context);
        evaluation.setLoan(loanApplication);
        evaluation.setDate(new DateTime());

        // run evaluators until first fail, or all succeed.
        for (LoanEvaluator riskEvaluator : evaluators) {
            log.debug("Executing evaluator {}", riskEvaluator);
            evaluation = riskEvaluator.evaluate(evaluation);
            if (!evaluation.accepted()) {
                break;
            }
        }
        log.debug("Loan evaluation result: {}", evaluation.getErrors());
        // return result;
        LoanEvaluationResult result = new LoanEvaluationResult();
        result.setResult(evaluation.accepted() ? Result.OK : Result.RISK_TOO_HIGH);
        result.setErrors(evaluation.getErrors());
        return result;
    }

}

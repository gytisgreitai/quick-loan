package eu.greitai.ql.evaluation;

import java.util.Set;

import org.joda.time.DateTime;

import com.google.common.collect.Sets;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.security.SecurityContext;

/**
 * 
 * Container of loan related data and current user context to be passed between multiple {@link LoanEvaluator}s
 * 
 * @author gytis
 */
public class LoanEvaluation {

    /**
     * Load we are evaluating
     */
    private LoanCalculation loan;

    /**
     * User that is trying to take out a loan
     */
    private SecurityContext context;

    /**
     * Date on which loan is being taken.
     */
    private DateTime date;

    /**
     * Errors, if any. If this lists is not empty, means that loan was reject.
     */
    private Set<String> errors;

    public LoanCalculation getLoan() {
        return loan;
    }

    public void setLoan(final LoanCalculation loan) {
        this.loan = loan;
    }

    public SecurityContext getContext() {
        return context;
    }

    public void setContext(final SecurityContext context) {
        this.context = context;
    }

    public Set<String> getErrors() {
        return errors;
    }

    public void setErrors(final Set<String> errors) {
        this.errors = errors;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(final DateTime date) {
        this.date = date;
    }

    /**
     * Rejects this loan.
     * 
     * @param error - why was loan rejected - string value for some sort of enum.
     */
    public void reject(final String error) {
        if (errors == null) {
            errors = Sets.newHashSet();
        }
        errors.add(error);
    }

    /**
     * Short hand to check if loan in question was accepted.
     * 
     * @return true if loan was accepted false otherwise.
     */
    public boolean accepted() {
        return errors == null || errors.size() == 0;
    }

    @Override
    public String toString() {
        return "RiskEvaluation [loan=" + loan + ", context=" + context + ", date=" + date + ", errors=" + errors + "]";
    }

}

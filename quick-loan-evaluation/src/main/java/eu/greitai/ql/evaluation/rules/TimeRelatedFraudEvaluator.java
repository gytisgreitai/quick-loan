package eu.greitai.ql.evaluation.rules;

import java.math.BigDecimal;

import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;

import eu.greitai.ql.evaluation.LoanEvaluation;
import eu.greitai.ql.evaluation.LoanEvaluator;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.settings.LoanSettings;

/**
 * 
 * Time related fraud evaluator - check that if time falls between given period
 * and amount is max - reject loan application.
 * 
 * @author gytis
 */
@Service
public class TimeRelatedFraudEvaluator implements LoanEvaluator, Ordered {

    @Autowired
    private SettingsService settings;

    // Time range when loan taking is considered
    private static final LocalTime STAR_TIME = new LocalTime(0, 0);
    private static final LocalTime END_TIME = new LocalTime(0, 2);

    @Override
    public LoanEvaluation evaluate(final LoanEvaluation evaluation) {
        BigDecimal amount = evaluation.getLoan().getAmount();

        // if are at our max amount
        if (amount.compareTo(settings.getAsDecimal(LoanSettings.AMOUNT_MAX)) >= 0) {
            LocalTime loanTime = evaluation.getDate().toLocalTime();
            // and we fall within danger zone - reject the application
            if (loanTime.isAfter(STAR_TIME) && loanTime.isBefore(END_TIME)) {
                evaluation.reject("MAX_AMOUNT_RISK_TIME");
            }
        }
        return evaluation;
    }

    @Override
    public int getOrder() {
        return 101;
    }

}

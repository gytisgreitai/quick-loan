package eu.greitai.ql.evaluation.rules;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import eu.greitai.ql.evaluation.LoanEvaluation;
import eu.greitai.ql.evaluation.LoanEvaluator;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.settings.LoanSettings;

/**
 * 
 * Checks that loan amount is within acceptable ranges.
 * 
 * @author gytis
 */
@Component
public class LoanRangeEvaluator implements LoanEvaluator, Ordered {

    @Autowired
    private SettingsService settings;

    @Override
    public LoanEvaluation evaluate(final LoanEvaluation evaluation) {
        BigDecimal amount = evaluation.getLoan().getAmount();
        if (amount == null) {
            evaluation.reject("NO_LOAN_AMOUNT");
        } else {
            BigDecimal min = settings.getAsDecimal(LoanSettings.AMOUNT_MIN);
            BigDecimal max = settings.getAsDecimal(LoanSettings.AMOUNT_MAX);
            if (amount.compareTo(min) < 0 || amount.compareTo(max) > 0) {
                evaluation.reject("NOT_WITHIN_RANGE");
            }
        }
        return evaluation;
    }

    @Override
    public int getOrder() {
        return 1;
    }
}

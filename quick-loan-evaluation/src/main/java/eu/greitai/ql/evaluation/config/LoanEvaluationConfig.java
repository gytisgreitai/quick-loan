package eu.greitai.ql.evaluation.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration module for quick-loan-evaluation project. Include this in projects
 * where you will access LoanEvaulationService.
 * 
 * @author gytis
 */
@Configuration
@ComponentScan({ "eu.greitai.ql.evaluation.rules", "eu.greitai.ql.evaluation.impl" })
public class LoanEvaluationConfig {

}

package eu.greitai.ql.services.tests.config;

import java.util.Map;

import com.google.common.collect.Maps;

import eu.greitai.ql.security.SecurityContext;

public class TestSecurityContext implements SecurityContext {

    @Override
    public Long getUserId() {
        return 1L;
    }

    @Override
    public Map<String, Object> getMetaData() {
        Map<String, Object> metaData = Maps.newHashMap();
        metaData.put("ipAddress", "127.0.0.1");
        return metaData;
    }

}

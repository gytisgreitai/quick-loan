package eu.greitai.ql.services.tests;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.security.SecurityContext;
import eu.greitai.ql.services.LoanService;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.services.tests.config.SettingsMock;
import eu.greitai.ql.settings.LoanSettings;

/**
 * 
 * Coverage for {@link LoanService#calculate(int, BigDecimal)}
 * 
 * @author gytis
 */
@ContextConfiguration(classes = { SettingsMock.class })
public class LoanCalculationTest extends BaseServiceTest {

    /**
     * Configuration is mocked to allow different settings to be passed in.
     */
    @Mock
    @Autowired
    private SettingsService settingsService;

    @Autowired
    private LoanService loanService;

    private final SecurityContext context = null;

    @Before
    public void before() {
        Mockito.reset(settingsService);
    }

    @Test
    public void loanServiceInvokesSettingsServiceMethod() {
        Mockito.when(settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)).thenReturn(BigDecimal.ONE);
        loanService.calculate(context, 1, new BigDecimal(1));
        Mockito.verify(settingsService, Mockito.times(1)).getAsDecimal(LoanSettings.DAILY_INTEREST_RATE);
    }

    @Test
    public void correctInterestRatesAreCalculated() {
        BigDecimal amount = new BigDecimal(550);
        int duration = 10;

        Mockito.when(settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)).thenReturn(BigDecimal.ONE);
        LoanCalculation loanResult = loanService.calculate(context, duration, amount);
        Mockito.verify(settingsService, Mockito.times(1)).getAsDecimal(LoanSettings.DAILY_INTEREST_RATE);

        // check that fields for setup.
        Assert.assertEquals("Expected duration to match", duration, loanResult.getDuration());
        Assert.assertEquals("Expected amount to match", amount, loanResult.getAmount());

        // check that calculations were ok - 550 * 10 * 1
        Assert.assertEquals("Invalid interest rates received", new BigDecimal("5500.00"), loanResult.getInterests());
        Assert.assertEquals("Invalid total received", new BigDecimal("6050.00"), loanResult.getTotal());

    }

    @Test
    public void correctScaleIsAppliedForInterests() {
        BigDecimal amount = new BigDecimal(99);
        int duration = 5;

        Mockito.when(settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)).thenReturn(new BigDecimal("0.345787879"));
        LoanCalculation loanResult = loanService.calculate(context, duration, amount);
        Mockito.verify(settingsService, Mockito.times(1)).getAsDecimal(LoanSettings.DAILY_INTEREST_RATE);

        // check that calculations were ok - 99 * 5 * 0.34578 = 171.165000105 = 171.17
        Assert.assertEquals("Invalid interest rates received", new BigDecimal("171.17"), loanResult.getInterests());
        Assert.assertEquals("Invalid total received", new BigDecimal("270.17"), loanResult.getTotal());

    }

    @Test
    public void correctEndDateIsSet() {

        Mockito.when(settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)).thenReturn(BigDecimal.ONE);
        LoanCalculation loanResult = loanService.calculate(context, 100, BigDecimal.ONE);
        Mockito.verify(settingsService, Mockito.times(1)).getAsDecimal(LoanSettings.DAILY_INTEREST_RATE);

        // after 100 days as of today
        Assert.assertEquals("Invalid interest rates received", new DateTime().plusDays(100).withTime(23, 59, 59, 999), loanResult.getReturnDate());

    }

}

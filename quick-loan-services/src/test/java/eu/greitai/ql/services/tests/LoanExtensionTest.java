package eu.greitai.ql.services.tests;

import java.math.BigDecimal;
import java.util.UUID;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.model.Loan;
import eu.greitai.ql.model.User;
import eu.greitai.ql.repository.LoanRepository;
import eu.greitai.ql.security.SecurityContext;
import eu.greitai.ql.services.AuditService;
import eu.greitai.ql.services.LoanService;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.services.tests.config.AuditMock;
import eu.greitai.ql.services.tests.config.SettingsMock;
import eu.greitai.ql.services.tests.config.TestSecurityContext;
import eu.greitai.ql.settings.LoanSettings;

/**
 * 
 * Coverage for {@link LoanService#extend(eu.greitai.ql.security.SecurityContext, java.util.UUID)}
 * 
 * TODO: add corner case coverage:
 * 1) Invalid inputs
 * 2) Loan not found
 * 
 * @author gytis
 */
@ContextConfiguration(classes = { SettingsMock.class, AuditMock.class })
public class LoanExtensionTest extends BaseServiceTest {

    @Mock
    @Autowired
    private SettingsService settingsService;

    @Mock
    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private LoanService loanService;

    @Mock
    @Autowired
    private AuditService auditService;

    private final SecurityContext context = new TestSecurityContext();


    @Before
    public void before() {
        Mockito.reset(settingsService);
        Mockito.reset(loanRepository);
        Mockito.reset(auditService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotExtendLoanThatDoesNotBelongToUser() {
        UUID loanGuid = UUID.randomUUID();
        Loan loan = new Loan();
        loan.setUser(new User());
        loan.getUser().setId(2L);

        //setup expectations
        Mockito.when(loanRepository.findCurrentByLoanGuid(loanGuid)).thenReturn(loan);

        // and check that our Precondition statement works.
        try {
            loanService.extend(context, loanGuid);
        } finally {
            Mockito.verify(loanRepository).findCurrentByLoanGuid(loanGuid);
            Mockito.verifyNoMoreInteractions(loanRepository);
        }
    }

    /**
     * Success scenario
     */
    @Test
    public void canExtendLoan() {

        // setup data
        UUID loanGuid = UUID.randomUUID();
        Loan loan = new Loan();
        loan.setLoanGuid(loanGuid);
        loan.setUser(new User());
        loan.getUser().setId(context.getUserId());
        loan.setCurrent(false);
        loan.setAmount(BigDecimal.TEN);
        loan.setInterests(BigDecimal.TEN);
        loan.setTotal(BigDecimal.TEN);
        loan.setIssueDate(new DateTime().minusDays(5));
        loan.setReturnDate(new DateTime().plusDays(5));

        // setup expectations
        Mockito.when(loanRepository.findCurrentByLoanGuid(loanGuid)).thenReturn(loan);
        Mockito.when(settingsService.getAsDecimal(LoanSettings.EXTENSION_MULTIPLIER)).thenReturn(BigDecimal.TEN);
        Mockito.when(settingsService.getAsInt(LoanSettings.EXTENSION_TIME)).thenReturn(7);

        // loan save expectations
        ArgumentCaptor<Loan> existing = ArgumentCaptor.forClass(Loan.class);
        ArgumentCaptor<Loan> extended = ArgumentCaptor.forClass(Loan.class);
        Mockito.when(loanRepository.save(existing.capture())).thenReturn(loan);
        Mockito.when(loanRepository.save(extended.capture())).thenAnswer(new Answer<Loan>() {
            @Override
            public Loan answer(final InvocationOnMock invocation) throws Throwable {
                Loan loanToReturn = (Loan) invocation.getArguments()[0];
                loanToReturn.setId(1L);
                return loanToReturn;
            }
        });

        // execute extension
        LoanCalculation result = loanService.extend(context, loanGuid);

        // verify
        Mockito.verify(loanRepository).findCurrentByLoanGuid(loanGuid);
        Mockito.verify(settingsService).getAsDecimal(LoanSettings.EXTENSION_MULTIPLIER);
        Mockito.verify(settingsService).getAsInt(LoanSettings.EXTENSION_TIME);
        Mockito.verify(loanRepository).save(loan);
        Mockito.verify(loanRepository).save(extended.getValue());

        // verify extension
        Loan created = extended.getValue();
        Assert.assertEquals("Expected date to be exteneded by 7 days", loan.getReturnDate().plusDays(7), created.getReturnDate());
        Assert.assertEquals("Expected interest rates to be extended by 10", new BigDecimal(100), created.getInterests());
        Assert.assertEquals("Expected total to be changed", new BigDecimal(110), created.getTotal());
        Assert.assertEquals("Expected amount to be the same", loan.getAmount(), created.getAmount());
        // verify audit service call
        Mockito.verify(auditService).audit(Matchers.eq(context), Matchers.eq(Event.LOAN_EXTENDED), Matchers.any(String.class));

        // verify transformation
        Assert.assertEquals("Expected return dates to match", created.getReturnDate(), result.getReturnDate());
        Assert.assertEquals("Expected total to match", created.getTotal(), result.getTotal());
        Assert.assertEquals("Expected interests to match", created.getInterests(), result.getInterests());
        Assert.assertEquals("Expected amount to match", created.getAmount(), result.getAmount());

    }
}

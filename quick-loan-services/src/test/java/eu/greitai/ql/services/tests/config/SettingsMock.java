package eu.greitai.ql.services.tests.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import eu.greitai.ql.services.SettingsService;

@Configuration
public class SettingsMock {

    @Bean
    @Primary
    public SettingsService settingsService() {
        return Mockito.mock(SettingsService.class);
    }
}

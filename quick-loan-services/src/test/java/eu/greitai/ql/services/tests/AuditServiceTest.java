package eu.greitai.ql.services.tests;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import eu.greitai.ql.model.Audit;
import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.repository.AuditRepository;
import eu.greitai.ql.security.SecurityContext;
import eu.greitai.ql.services.AuditService;
import eu.greitai.ql.services.impl.AuditServiceImpl;
import eu.greitai.ql.services.tests.config.SettingsMock;
import eu.greitai.ql.services.tests.config.TestSecurityContext;

/**
 * 
 * Tests for {@link AuditServiceImpl}
 * 
 * @author gytis
 */
@ContextConfiguration(classes = { SettingsMock.class })
public class AuditServiceTest extends BaseServiceTest {


    @Autowired
    private AuditService auditService;

    @Mock
    @Autowired
    private AuditRepository auditRepository;

    private final SecurityContext context = new TestSecurityContext();

    @Before
    public void before() {
        Mockito.reset(auditRepository);
    }

    @Test
    public void auditCanBeCorrectlySaved() {
        // setup expectations
        ArgumentCaptor<Audit> audit = ArgumentCaptor.forClass(Audit.class);
        Mockito.when(auditRepository.save(audit.capture())).thenReturn(new Audit());

        auditService.audit(context, Event.LOAN_EXTENDED, null);

        //verify correct calls
        Mockito.verify(auditRepository).save(audit.getValue());
        Mockito.verifyNoMoreInteractions(auditRepository);

        //verify audit was transformed correctly
        Audit captured = audit.getValue();
        Assert.assertEquals("Expected to have same event type", Event.LOAN_EXTENDED, captured.getEvent());
        Assert.assertEquals("Expected to have same ip address", "127.0.0.1", captured.getIpAddress());
        Assert.assertEquals("Expected to get same user id", context.getUserId(), captured.getUserId());

    }

    @Test
    public void serviceCorrectlyPassesDataThrough() {
        DateTime start = new DateTime();
        DateTime end = new DateTime();
        Long count = 1L;

        Mockito.when(auditRepository.countByEventTypeForIpPerPeriod(
                Matchers.eq(Event.LOAN_EXTENDED),
                Matchers.eq("1"),
                Matchers.eq(start),
                Matchers.eq(end))).thenReturn(count);

        Long resultCount = auditRepository.countByEventTypeForIpPerPeriod(Event.LOAN_EXTENDED, "1", start, end);

        Mockito.verify(auditRepository).countByEventTypeForIpPerPeriod(Event.LOAN_EXTENDED, "1", start, end);
        Assert.assertEquals("Cound should match", count, resultCount);
    }

}

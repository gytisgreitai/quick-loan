package eu.greitai.ql.services.tests.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.greitai.ql.repository.AuditRepository;
import eu.greitai.ql.repository.LoanRepository;
import eu.greitai.ql.repository.UserRepository;
import eu.greitai.ql.services.LoanEvaluationService;

@Configuration
public class ServicesTestConfig {

    @Bean
    public LoanEvaluationService loanEvaluationService() {
        return Mockito.mock(LoanEvaluationService.class);
    }

    @Bean
    public UserRepository userRepository() {
        return Mockito.mock(UserRepository.class);
    }

    @Bean
    public LoanRepository loanRepository() {
        return Mockito.mock(LoanRepository.class);
    }

    @Bean
    public AuditRepository auditRepository() {
        return Mockito.mock(AuditRepository.class);
    }
}

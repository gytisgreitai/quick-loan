package eu.greitai.ql.services.tests;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.greitai.ql.services.config.ServicesConfig;
import eu.greitai.ql.services.tests.config.ServicesTestConfig;

/**
 * 
 * Base test runner class for services module testing. Simply extend your class from this one
 * 
 * @author gytis
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServicesTestConfig.class, ServicesConfig.class })
public abstract class BaseServiceTest {

}

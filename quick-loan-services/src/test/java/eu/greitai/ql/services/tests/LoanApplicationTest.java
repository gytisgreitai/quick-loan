package eu.greitai.ql.services.tests;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import eu.greitai.ql.dto.LoanApplicationResult;
import eu.greitai.ql.dto.LoanApplicationResult.Status;
import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.dto.LoanEvaluationResult;
import eu.greitai.ql.dto.LoanEvaluationResult.Result;
import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.model.Loan;
import eu.greitai.ql.model.User;
import eu.greitai.ql.repository.LoanRepository;
import eu.greitai.ql.repository.UserRepository;
import eu.greitai.ql.security.SecurityContext;
import eu.greitai.ql.services.AuditService;
import eu.greitai.ql.services.LoanEvaluationService;
import eu.greitai.ql.services.LoanService;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.services.tests.config.AuditMock;
import eu.greitai.ql.services.tests.config.SettingsMock;
import eu.greitai.ql.services.tests.config.TestSecurityContext;
import eu.greitai.ql.settings.LoanSettings;

/**
 * 
 * Coverage for {@link LoanService#apply(eu.greitai.ql.security.SecurityContext, int, BigDecimal, BigDecimal)}
 * 
 * TODO: cover corner case scenarios:
 * 1) Invalid security context
 * 2) Check all transformation logic
 * 
 * @author gytis
 */
@ContextConfiguration(classes = { SettingsMock.class, AuditMock.class })
public class LoanApplicationTest extends BaseServiceTest {

    @Mock
    @Autowired
    private SettingsService settingsService;

    @Mock
    @Autowired
    private AuditService auditService;

    @Mock
    @Autowired
    private LoanEvaluationService loanEvaluationService;

    @Mock
    @Autowired
    private LoanRepository loanRepository;

    @Mock
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoanService loanService;

    private final SecurityContext context = new TestSecurityContext();

    @Before
    public void before() {
        Mockito.reset(settingsService);
        Mockito.reset(loanEvaluationService);
        Mockito.reset(loanRepository);
        Mockito.reset(userRepository);
        Mockito.reset(auditService);
    }

    @Test
    public void seenPriceMismatchDoesNotCallAnyServices() {
        // setup expectations
        Mockito.when(settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)).thenReturn(BigDecimal.ONE);

        // execute - total should be 20, not 10, therefore we should fail.
        LoanApplicationResult application = loanService.apply(context, 1, BigDecimal.TEN, BigDecimal.TEN);

        Assert.assertEquals("Expected loan duration to match", Status.PRICE_MISMATCH, application.getStatus());
        Mockito.verifyNoMoreInteractions(loanEvaluationService);
        Mockito.verifyNoMoreInteractions(loanRepository);
    }

    @Test
    public void loanEvaluationServiceIsCalledWithCorrectArguments() {
        // setup expectations
        Mockito.when(settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)).thenReturn(BigDecimal.ONE);

        ArgumentCaptor<LoanCalculation> loan = ArgumentCaptor.forClass(LoanCalculation.class);
        Mockito.when(loanEvaluationService.evaluate(Matchers.eq(context), loan.capture())).thenReturn(new LoanEvaluationResult());

        // execute - total 20 = 10 for loan + 10 for interest rates.
        loanService.apply(context, 1, BigDecimal.TEN, new BigDecimal(20));

        Mockito.verify(loanEvaluationService).evaluate(Matchers.eq(context), loan.capture());
        Mockito.verifyNoMoreInteractions(loanEvaluationService);
        Assert.assertEquals("Expected loan duration to match", 1, loan.getValue().getDuration());
        Assert.assertEquals("Expected loan amount to match", BigDecimal.TEN, loan.getValue().getAmount());
    }

    @Test
    public void rejectedEvaluationDoesNotSaveLoanToTheDatabase() {
        // setup expectations
        Mockito.when(settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)).thenReturn(BigDecimal.ONE);

        LoanEvaluationResult evaluation = new LoanEvaluationResult();
        evaluation.setResult(Result.RISK_TOO_HIGH);
        Mockito.when(loanEvaluationService.evaluate(Matchers.eq(context), Matchers.any(LoanCalculation.class))).thenReturn(evaluation);

        // execute - total 20 = 10 for loan + 10 for interest rates.
        LoanApplicationResult application = loanService.apply(context, 1, BigDecimal.TEN, new BigDecimal(20));

        // verify mocks
        Mockito.verify(loanEvaluationService).evaluate(Matchers.eq(context), Matchers.any(LoanCalculation.class));
        Mockito.verifyNoMoreInteractions(loanEvaluationService);
        Assert.assertEquals("Expected to have a declined status", Status.DECLINED, application.getStatus());

        // verify no save to database.
        Mockito.verifyNoMoreInteractions(loanRepository);
    }

    /**
     * Success scenario
     */
    @Test
    public void acceptedEvaluationSubmitsLoanToDatabase() {
        // setup expectations
        Mockito.when(settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)).thenReturn(BigDecimal.ONE);

        LoanEvaluationResult evaluation = new LoanEvaluationResult();
        evaluation.setResult(Result.OK);
        Mockito.when(loanEvaluationService.evaluate(Matchers.eq(context), Matchers.any(LoanCalculation.class))).thenReturn(evaluation);

        //expect that we should get user from context.
        User user = new User();
        Mockito.when(userRepository.findOne(Matchers.eq(context.getUserId()))).thenReturn(user);

        //and expect loan save
        Loan savedLoan = new Loan();
        savedLoan.setId(1L);
        ArgumentCaptor<Loan> loan = ArgumentCaptor.forClass(Loan.class);
        Mockito.when(loanRepository.save(loan.capture())).thenReturn(savedLoan);

        // execute - total 20 = 10 for loan + 10 for interest rates.
        LoanApplicationResult application = loanService.apply(context, 1, BigDecimal.TEN, new BigDecimal(20));

        // verify evaluation mocks
        Mockito.verify(loanEvaluationService).evaluate(Matchers.eq(context), Matchers.any(LoanCalculation.class));
        Mockito.verifyNoMoreInteractions(loanEvaluationService);
        //verify user and repository mocks
        Mockito.verify(userRepository).findOne(Matchers.eq(context.getUserId()));
        Mockito.verifyNoMoreInteractions(userRepository);
        Mockito.verify(loanRepository).save(loan.capture());
        Mockito.verifyNoMoreInteractions(loanRepository);
        //verify audit service call
        Mockito.verify(auditService).audit(Matchers.eq(context), Matchers.eq(Event.LOAN_TAKEN), Matchers.any(String.class));

        // verify that saved loan was transformed correctly
        LoanCalculation returned = application.getLoan();
        Loan saved = loan.getValue();

        Assert.assertEquals("Saved and returned amount should match", returned.getAmount(), saved.getAmount());
        Assert.assertEquals("Saved and returned total should match", returned.getTotal(), saved.getTotal());
        Assert.assertEquals("Saved and returned interests should match", returned.getInterests(), saved.getInterests());
        Assert.assertEquals("Saved and returned return date should match", returned.getReturnDate(), saved.getReturnDate());
    }

}

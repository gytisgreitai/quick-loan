package eu.greitai.ql.services.tests;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.settings.LoanSettings;
import eu.greitai.ql.settings.Setting;

/**
 * 
 * Few simple tests for {@link SettingsService} implementation.
 * 
 * @author gytis
 */
public class SettingsTest extends BaseServiceTest {

    @Autowired
    private SettingsService settingsService;

    @Test
    public void bigDecimalCanBeRetrieved() {
        Assert.assertNotNull("Expected to get integer", settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE));
    }

    @Test
    public void ensureThatDailyRateHasGoodPrecision() {
        Assert.assertEquals("Expected to have high precission", 10, settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE).scale());
    }

    @Test
    public void ensureThatDailyInterestIsGreatherThanZero() {
        Assert.assertEquals("DAILY_INTEREST_RATE is not greather than zero",
                1,
                settingsService.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE).compareTo(BigDecimal.ZERO));
    }

    @Test
    public void integerCanBeRetrieved() {
        Assert.assertNotNull("Expected to get integer", settingsService.getAsInt(LoanSettings.DURATION_MIN));
    }
    
    @Test
    public void objectCanBeRetrieved() {
        Assert.assertNotNull("Expected to get an object", settingsService.get(LoanSettings.DURATION_MIN));
    }
    
    @Test
    public void nonExistingSettingReturnsNull() {
        Assert.assertNull("non existing setting should retrieve null", settingsService.get(TestSetting.NON_EXISTING));
        Assert.assertNull("non existing setting should retrieve null", settingsService.getAsInt(TestSetting.NON_EXISTING));
        Assert.assertNull("non existing setting should retrieve null", settingsService.getAsDecimal(TestSetting.NON_EXISTING));
    }

    private static enum TestSetting implements Setting {
        NON_EXISTING
    }

}

package eu.greitai.ql.services.tests.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import eu.greitai.ql.services.AuditService;

@Configuration
public class AuditMock {
    @Bean
    @Primary
    public AuditService auditService() {
        return Mockito.mock(AuditService.class);
    }
}

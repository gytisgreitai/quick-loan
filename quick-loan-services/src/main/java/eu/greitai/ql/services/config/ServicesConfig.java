package eu.greitai.ql.services.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * Our services module spring configuration.
 * 
 * @author gytis
 */
@Configuration
@ComponentScan("eu.greitai.ql.services.impl")
public class ServicesConfig {

}

package eu.greitai.ql.services.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.settings.Constants;
import eu.greitai.ql.settings.LoanSettings;
import eu.greitai.ql.settings.Setting;

@Service
public class SettingsServiceImpl implements SettingsService {

    public final static Map<Setting, Object> ALL = new HashMap<>();
    static {
        ALL.put(LoanSettings.AMOUNT_MIN, new BigDecimal("100")); // 100$
        ALL.put(LoanSettings.AMOUNT_MAX, new BigDecimal("1500")); // 1500$
        ALL.put(LoanSettings.AMOUNT_INITIAL, new BigDecimal("300")); // 300$
        ALL.put(LoanSettings.EXTENSION_MULTIPLIER, new BigDecimal("1.5")); // multiplier
        ALL.put(LoanSettings.EXTENSION_TIME, 7); // 7 days
        ALL.put(LoanSettings.DURATION_MIN, 10); // 10 days
        ALL.put(LoanSettings.DURATION_MAX, 60); // 60 days
        ALL.put(LoanSettings.DURATION_INITIAL, 15); // 15 dates
        ALL.put(LoanSettings.YEARLY_INTEREST, 150); // yearly interests percentage
        ALL.put(LoanSettings.DAYS_PER_YEAR, 365); // how many days in a year? not very accurate
        ALL.put(LoanSettings.DAILY_INTEREST_RATE,
                new BigDecimal((Integer) ALL.get(LoanSettings.YEARLY_INTEREST))
                        .setScale(10)
                        .divide(new BigDecimal("100.00"), Constants.ALGO_ROUNDING_MODE)
                        .divide(new BigDecimal((Integer) ALL.get(LoanSettings.DAYS_PER_YEAR)), Constants.ALGO_ROUNDING_MODE));
    }

    @Override
    public BigDecimal getAsDecimal(final Setting setting) {
        return (BigDecimal) ALL.get(setting);
    }

    @Override
    public Integer getAsInt(final Setting setting) {
        return (Integer) ALL.get(setting);
    }

    @Override
    public Object get(final Setting setting) {
        return ALL.get(setting);
    }

    @Override
    public Map<Setting, Object> getAll() {
        return Maps.newHashMap(ALL);
    }
}

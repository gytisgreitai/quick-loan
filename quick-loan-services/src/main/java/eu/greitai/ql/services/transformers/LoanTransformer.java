package eu.greitai.ql.services.transformers;

import java.util.UUID;

import org.joda.time.DateTime;
import org.joda.time.Days;

import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.model.Loan;
import eu.greitai.ql.model.Loan.Type;

/**
 * 
 * Helper class to provide conversions {@link Loan} <-> {@link LoanCalculation}
 * 
 * @author gytis
 */
public class LoanTransformer {

    public static Loan toModel(final LoanCalculation calculation) {
        Loan loan = new Loan();
        loan.setAmount(calculation.getAmount());
        loan.setCurrent(true);
        loan.setInterests(calculation.getInterests());
        loan.setIssueDate(new DateTime());
        loan.setLoanGuid(UUID.randomUUID());
        loan.setReturnDate(calculation.getReturnDate());
        loan.setTotal(calculation.getTotal());
        loan.setType(Type.ORIGINAL);
        return loan;
    }

    public static LoanCalculation toDto(final Loan loan) {
        LoanCalculation calc = new LoanCalculation();
        calc.setAmount(loan.getAmount());
        calc.setDuration(Days.daysBetween(loan.getIssueDate(), loan.getReturnDate()).getDays());
        calc.setInterests(loan.getInterests());
        calc.setReturnDate(loan.getReturnDate());
        calc.setTotal(loan.getTotal());
        return calc;
    }
}

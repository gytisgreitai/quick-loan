package eu.greitai.ql.services.impl;

import java.math.BigDecimal;
import java.util.UUID;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

import eu.greitai.ql.dto.LoanApplicationResult;
import eu.greitai.ql.dto.LoanApplicationResult.Status;
import eu.greitai.ql.dto.LoanCalculation;
import eu.greitai.ql.dto.LoanEvaluationResult;
import eu.greitai.ql.dto.LoanEvaluationResult.Result;
import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.model.Loan;
import eu.greitai.ql.model.Loan.Type;
import eu.greitai.ql.repository.LoanRepository;
import eu.greitai.ql.repository.UserRepository;
import eu.greitai.ql.security.SecurityContext;
import eu.greitai.ql.services.AuditService;
import eu.greitai.ql.services.LoanEvaluationService;
import eu.greitai.ql.services.LoanService;
import eu.greitai.ql.services.SettingsService;
import eu.greitai.ql.services.transformers.LoanTransformer;
import eu.greitai.ql.settings.Constants;
import eu.greitai.ql.settings.LoanSettings;

@Service
public class LoanServiceImpl implements LoanService {

    @Autowired
    private SettingsService settings;

    @Autowired
    private LoanEvaluationService loanRiskService;

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuditService auditService;

    @Override
    public LoanCalculation calculate(final SecurityContext context, final int duration, final BigDecimal amount) {
        LoanCalculation result = new LoanCalculation();

        // calculate interests and totals
        BigDecimal interests = settings.getAsDecimal(LoanSettings.DAILY_INTEREST_RATE)
                .multiply(amount)
                .multiply(new BigDecimal(duration))
                .setScale(Constants.VISIBLE_SCALE, Constants.VISIBLE_ROUNDING_MODE);
        BigDecimal total = amount.add(interests);

        // set it
        result.setAmount(amount);
        result.setDuration(duration);
        result.setTotal(total);
        result.setInterests(interests);
        result.setReturnDate(new DateTime().plusDays(duration).withTime(23, 59, 59, 999));
        return result;

    }

    @Override
    @Transactional
    public LoanApplicationResult apply(
            final SecurityContext context,
            final int duration, final BigDecimal amount,
            final BigDecimal seenPrice) {

        Preconditions.checkNotNull(context.getUserId(), "Only logged in user can apply for a loan");

        LoanApplicationResult application = new LoanApplicationResult();

        // calculate the loan.
        LoanCalculation result = calculate(context, duration, amount);
        application.setLoan(result);

        // verify that prices match.
        if (result.getTotal().compareTo(seenPrice) != 0) {
            application.setStatus(Status.PRICE_MISMATCH);
            return application;
        }

        // check risk.
        LoanEvaluationResult risk = loanRiskService.evaluate(context, result);
        if (risk.getResult() != Result.OK) {
            application.setStatus(Status.DECLINED);
            return application;
        }

        //save loan and return.
        Loan loan = LoanTransformer.toModel(result);
        loan.setUser(userRepository.findOne(context.getUserId()));
        loan = loanRepository.save(loan);

        // audit event
        auditService.audit(context, Event.LOAN_TAKEN, loan.getId().toString());
        return application;
    }

    @Override
    @Transactional
    public LoanCalculation extend(final SecurityContext context, final UUID loanGuid) {
        Preconditions.checkNotNull(context.getUserId(), "Only logged in user extend a loan");
        Preconditions.checkNotNull(loanGuid, "loan guid must be provided");

        Loan loan = loanRepository.findCurrentByLoanGuid(loanGuid);
        Preconditions.checkArgument(loan.getUser().getId().equals(context.getUserId()), "Tried to access another users loan " + context);

        // create loan extension
        Loan extended = new Loan();
        extended.setAmount(loan.getAmount());
        extended.setCurrent(true);
        extended.setInterests(loan.getInterests().multiply(settings.getAsDecimal(LoanSettings.EXTENSION_MULTIPLIER)));
        extended.setIssueDate(loan.getIssueDate());
        extended.setLoanGuid(loan.getLoanGuid());
        extended.setReturnDate(loan.getReturnDate().plusDays(settings.getAsInt(LoanSettings.EXTENSION_TIME)));
        extended.setTotal(extended.getAmount().add(extended.getInterests()));
        extended.setType(Type.EXTENSION);
        extended.setUpdateDate(new DateTime());
        extended.setUser(loan.getUser());

        // commit it
        loan.setCurrent(false);
        loanRepository.save(loan);
        extended = loanRepository.save(extended);

        // audit event
        auditService.audit(context, Event.LOAN_EXTENDED, extended.getId().toString());

        // and return
        return LoanTransformer.toDto(extended);
    }
}

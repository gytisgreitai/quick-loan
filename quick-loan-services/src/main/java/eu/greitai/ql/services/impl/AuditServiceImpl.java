package eu.greitai.ql.services.impl;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.greitai.ql.model.Audit;
import eu.greitai.ql.model.Audit.Event;
import eu.greitai.ql.repository.AuditRepository;
import eu.greitai.ql.security.SecurityContext;
import eu.greitai.ql.services.AuditService;

@Service
public class AuditServiceImpl implements AuditService {

    @Autowired
    private AuditRepository auditRepository;

    @Override
    @Transactional
    public void audit(final SecurityContext context, final Event event, final String data) {
        Audit audit = new Audit();
        audit.setDate(new DateTime());
        audit.setEvent(event);
        audit.setUserId(context.getUserId());
        audit.setIpAddress(context.getMetaData().get("ipAddress").toString());
        audit.setMetaData(data);

        auditRepository.save(audit);

    }

    @Override
    @Transactional(readOnly = true)
    public Long getCountByEventTypeForIpPerPeriod(final Event event, final String ipAddress, final DateTime start, final DateTime end) {
        return auditRepository.countByEventTypeForIpPerPeriod(event, ipAddress, start, end);
    }

}
